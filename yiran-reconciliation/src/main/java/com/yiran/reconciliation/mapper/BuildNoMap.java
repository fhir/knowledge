
package com.yiran.reconciliation.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.huohuzhihui.common.annotation.DataSource;
import com.huohuzhihui.common.enums.DataSourceType;

@Mapper
@DataSource(value = DataSourceType.SHOP)
public interface BuildNoMap {

    public String getSeqNextValue(String seqName);

}
