package com.yiran.reconciliation.domain;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.huohuzhihui.common.core.domain.BaseEntity;

import lombok.Data;

/**
 * 对账批次表 reconciliation_account_check_batch
 * 
 * @author yiran
 * @date 2019-09-20
 */
@Data
public class ReconciliationAccountCheckBatch extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 编号 */
	private int id;
	/** version */
	private Integer version;	
	
	/** 状态 ACTIVE 激活  UNACTIVE冻结 */
	private String status;
	
	/** 对账批次号 */
	private String batchNo;
	/** 账单时间(账单交易发生时间) */
	private Date billDate;
	/** 账单类型 */
	private String billType;
	/** 批次处理状态, S已处理, A未处理 */
	private String handleStatus;
	/** 银行类型 */
	private String bankType;
	/** 所有差错总单数 */
	private Integer mistakeCount;
	/** 待处理的差错总单数 */
	private Integer unhandleMistakeCount;
	/** 平台总交易单数 */
	private Integer tradeCount;
	/** 银行总交易单数 */
	private Integer bankTradeCount;
	/** 平台交易总金额 */
	private BigDecimal tradeAmount;
	/** 银行交易总金额 */
	private BigDecimal bankTradeAmount;
	/** 平台退款总金额 */
	private BigDecimal refundAmount;
	/** 银行退款总金额 */
	private BigDecimal bankRefundAmount;
	/** 平台总手续费 */
	private BigDecimal bankFee;
	/** 原始对账文件存放地址 */
	private String orgCheckFilePath;
	/** 解析后文件存放地址 */
	private String releaseCheckFilePath;
	/** 解析状态 */
	private String releaseStatus;
	/** 解析检查失败的描述信息  */
	private String checkFailMsg;
	/** 银行返回的错误信息 */
	private String bankErrMsg;

	
    
}
