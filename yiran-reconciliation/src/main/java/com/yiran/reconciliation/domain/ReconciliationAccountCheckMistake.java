package com.yiran.reconciliation.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.huohuzhihui.common.core.domain.BaseEntity;

import lombok.Data;

/**
 * 对账差错表 reconciliation_account_check_mistake
 * 
 * @author yiran
 * @date 2019-09-20
 */
@Data
public class ReconciliationAccountCheckMistake extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 编码 */
	private int id;
	/** 版本 */
	private Integer version;	
	
	/** 状态 */
	private String status;

	/** 对账批次号 */
	private String accountCheckBatchNo;
	/** 账单日期 */
	private Date billDate;
	/** 银行类型 */
	private String bankType;
	/** 下单时间  */
	private Date orderTime;
	/** merchantName */
	private String merchantName;
	/** 商家编号 */
	private String merchantNo;
	/** 商家订单号 */
	private String orderNo;
	/** 平台交易时间  */
	private Date tradeTime;
	/** 平台流水号 */
	private String trxNo;
	/** 平台交易金额 */
	private BigDecimal orderAmount;
	/** 平台退款金额  */
	private BigDecimal refundAmount;
	/** 平台交易状态 */
	private String tradeStatus;
	/** 平台手续费 */
	private BigDecimal fee;
	/** 银行交易时间 */
	private Date bankTradeTime;
	/** 银行订单号 */
	private String bankOrderNo;
	/** 银行流水号 */
	private String bankTrxNo;
	/** 银行交易状态 */
	private String bankTradeStatus;
	/** 银行交易金额 */
	private BigDecimal bankAmount;
	/** 银行退款金额 */
	private BigDecimal bankRefundAmount;
	/** 银行手续费 */
	private BigDecimal bankFee;
	/** 差错类型 */
	private String errType;
	/** handleStatus */
	private String handleStatus;
	/** 处理结果 */
	private String handleValue;
	/** 处理备注 */
	private String handleRemark;
	/** 操作人 */
	private String operatorName;
	/** 操作账户 */
	private String operatorAccountNo;

}
