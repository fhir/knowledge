package com.yiran.reconciliation.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.core.domain.BaseEntity;

import lombok.Data;

import java.util.Date;

/**
 * 对账渠道设置表 reconciliation_channel_setting
 * 
 * @author yiran
 * @date 2019-09-20
 */
@Data
public class ReconciliationChannelSetting extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 编号 */
	private Integer id;
	/** 支付渠道名称 */
	private String channelName;
	/** 渠道编号 */
	private String fundChannelCode;
	/**
	 * 机构编码
	 */
	private String instCode;
	/** 描述 */
	private String reconcileDesc;
	/** 是否有效 */
	private String status;
	/** 对账周期 */
	private Integer billDay;
	/** 扩展信息 */
	private String extend;	

}
