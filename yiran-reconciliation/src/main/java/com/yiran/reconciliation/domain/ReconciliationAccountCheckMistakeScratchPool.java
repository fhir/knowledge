package com.yiran.reconciliation.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.core.domain.BaseEntity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 差错暂存池表 reconciliation_account_check_mistake_scratch_pool
 * 
 * @author yiran
 * @date 2019-09-20
 */
@Data
public class ReconciliationAccountCheckMistakeScratchPool extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 编号 */
	private int id;
	/** 版本 */
	private Integer version;
	
	/** 商品名称 */
	private String productName;
	/** 商户订单号 */
	private String merchantOrderNo;
	/** 支付流水号 */
	private String trxNo;
	/** 银行订单号 */
	private String bankOrderNo;
	/** 银行流水号 */
	private String bankTrxNo;
	/** 订单金额 */
	private BigDecimal orderAmount;
	/** 平台收入 */
	private BigDecimal platIncome;
	/** 费率 */
	private BigDecimal feeRate;
	/** 平台成本 */
	private BigDecimal platCost;
	/** 平台利润 */
	private BigDecimal platProfit;
	/** 状态(参考枚举:paymentrecordstatusenum) */
	private String status;
	/** 支付通道编号 */
	private String fundChannelCode;
	/** 支付通道名称 */
	private String fundChannelName;
	/** 支付成功时间 */
	private Date paySuccessTime;
	/** 完成时间 */
	private Date completeTime;
	/** 是否退款(100:是,101:否,默认值为:101) */
	private String isRefund;
	/** 退款次数(默认值为:0) */
	private Integer refundTimes;
	/** 成功退款总金额 */
	private BigDecimal successRefundAmount;
	
	/** 对账批次号 */
	private String batchNo;
	/** 对账时间 */
	private Date billDate;

}
