package com.yiran.member.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.core.domain.BaseEntity;

import lombok.Data;

import java.util.Date;

/**
 * 配置表 member_td_config
 * 
 * @author yiran
 * @date 2019-03-30
 */
@Data
public class MemberTdConfig extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 主键ID */
	private Integer id;
	/** 配置类型 */
	private String type;
	/** 配置子类型 */
	private String subType;
	/** 配置值 */
	private String value;
	/** 状态(0不可用 1可用) */
	private Integer status;
	/** 类型描述 */
	private String typeDesc;
	/** 值描述 */
	private String valueDesc;
	/** 顺序号 */
	private Integer orderNo;
	
	private String memo;
	
	/** 扩展字段1 */
	private String ext1;
	/** 扩展字段2 */
	private String ext2;
	/** 扩展字段3 */
	private String ext3;
	
}
