package com.yiran.member.domain;

import java.io.Serializable;
import java.util.Date;

import com.huohuzhihui.common.core.domain.BaseEntity;

import lombok.Data;

/**
 *合作商户会员信息
 */
@Data
public class CoopMember extends BaseEntity {

	/**
	 * 合作商户的会员ID
	 */
	private String coopMemberId;
	/**
	 * 登陆名称(手机号码或邮箱)
	 */
	private String loginName;
	/**
	 * 登陆名称的来源类型(1邮箱 2手机)
	 */
	private int sourceType;
	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 登录密码
	 */
	private String password;
	/**
	 * 真实姓名
	 */
	private String realName;
	/**
	 * 身份证号码
	 */
	private String idCard;
	
	/**
	 * 备注信息
	 */
	private String memo;
	
	
	/**
	 * 快捷通ID
	 */
	private String kjtId;
	
	
	/**
	 * 快捷通账户
	 */
	private String kjtAccount;
	
	/**
	 * 加密的手机号码
	 */
	private String encryptMobile;
	/**
	 * 加密的邮箱
	 */
	private String encryptEmail;
	
	/**
	 * 状态：1.正常，2.已修改登录名就代笔这个账户失效了，默认为1
	 */
	private int status;
	
	/**
	 * 会员类型，1:个人会员 2：企业会员
	 */
	private Integer memberType;
	
	
}
