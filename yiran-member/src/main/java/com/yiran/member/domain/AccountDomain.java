package com.yiran.member.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;


import cn.hutool.core.math.Money;
import lombok.Data;

import com.huohuzhihui.common.core.domain.BaseEntity;
import com.yiran.member.enums.AccountAttributeEnum;
import com.yiran.member.enums.AccountCategoryEnum;



/**
 * <p>账户域对象</p>
 */
@Data
public class AccountDomain extends BaseEntity {
 

    /**
     * 会员编号
     */
    private String               memberId;

    /**
     * 账户编号
     */
    private String               accountId;

    /**
     * 账户名称
     */
    private String               accountName;

    /**
     * 账户类型
     */
    private Long                 accountType;

    /**
     * 账户余额
     */
    private Money                balance;

    /**
     * 账户可用余额
     */
    private Money                availableBalance;

    /**
     * 账户冻结余额
     */
    private Money                frozenBalance;

    /**
     * 可提现金额，信用账户现金额度
     */
    private Money                withdrawBalance;

    /**
     * 激活状态 0:未激活    1:已激活
     */
    private Long                 activateStatus;

    /**
     * 冻结状态    0:未冻结    1:止出    2:止入    3:双向冻结(锁定)
     */
    private Long                 freezeStatus;

    /**
     * 销户状态 0:未销户    1:已销户    2:已结转长期不动户
     */
    private Long                 lifeCycleStatus;

    /**
     * 更后更新时间
     */
    private Date                 lastUpdatetime;

    /**
     * 币种
     */
    private String               currencyCode;

    /**
     * 扩展字段
     */
    private Map<String, Object>  extention;

    /**
     * 账户请求号
     */
    private String               originalRequestNo;

    
    /**
     * 账户属性
     */
    private AccountAttributeEnum accountAttribute;

    /**
     * 账户分类
     */
    private AccountCategoryEnum  cateEnum;

    /**
     * 账户类型，accountType 切换到 accountTypeId
     */
    private String               accountTypeId;
   
}
