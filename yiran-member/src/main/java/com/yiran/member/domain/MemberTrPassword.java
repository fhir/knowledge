package com.yiran.member.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 会员支付密码表 member_tr_password
 * 
 * @author yiran
 * @date 2019-03-30
 */
public class MemberTrPassword extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 主键 */
	private Integer id;
	/** 操作员编号 */
	private String operatorId;
	/** 账户ID */
	private String accountId;
	/** 密码 */
	private String password;
	
	/** 备注信息 */
	private String memo;
	/** 密码状态 0.初始 1.正常 */
	private Integer status;
	/** 手机端密码 */
	private String mpassword;

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getId() 
	{
		return id;
	}
	public void setOperatorId(String operatorId) 
	{
		this.operatorId = operatorId;
	}

	public String getOperatorId() 
	{
		return operatorId;
	}
	public void setAccountId(String accountId) 
	{
		this.accountId = accountId;
	}

	public String getAccountId() 
	{
		return accountId;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}

	public String getPassword() 
	{
		return password;
	}
	

	public void setMemo(String memo) 
	{
		this.memo = memo;
	}

	public String getMemo() 
	{
		return memo;
	}
	public void setStatus(Integer status) 
	{
		this.status = status;
	}

	public Integer getStatus() 
	{
		return status;
	}
	public void setMpassword(String mpassword) 
	{
		this.mpassword = mpassword;
	}

	public String getMpassword() 
	{
		return mpassword;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("operatorId", getOperatorId())
            .append("accountId", getAccountId())
            .append("password", getPassword())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())            
            .append("memo", getMemo())
            .append("status", getStatus())
            .append("mpassword", getMpassword())
            .toString();
    }
}
