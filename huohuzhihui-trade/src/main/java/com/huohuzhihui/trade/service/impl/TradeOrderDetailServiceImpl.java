package com.huohuzhihui.trade.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.trade.mapper.TradeOrderDetailMapper;
import com.huohuzhihui.trade.domain.TradeOrderDetail;
import com.huohuzhihui.trade.service.ITradeOrderDetailService;

/**
 * 订单明细Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-14
 */
@Service
public class TradeOrderDetailServiceImpl implements ITradeOrderDetailService 
{
    @Autowired
    private TradeOrderDetailMapper tradeOrderDetailMapper;

    /**
     * 查询订单明细
     * 
     * @param id 订单明细ID
     * @return 订单明细
     */
    @Override
    public TradeOrderDetail selectTradeOrderDetailById(Long id)
    {
        return tradeOrderDetailMapper.selectTradeOrderDetailById(id);
    }

    /**
     * 查询订单明细列表
     * 
     * @param tradeOrderDetail 订单明细
     * @return 订单明细
     */
    @Override
    public List<TradeOrderDetail> selectTradeOrderDetailList(TradeOrderDetail tradeOrderDetail)
    {
        return tradeOrderDetailMapper.selectTradeOrderDetailList(tradeOrderDetail);
    }

    /**
     * 新增订单明细
     * 
     * @param tradeOrderDetail 订单明细
     * @return 结果
     */
    @Override
    public int insertTradeOrderDetail(TradeOrderDetail tradeOrderDetail)
    {
        tradeOrderDetail.setCreateTime(DateUtils.getNowDate());
        return tradeOrderDetailMapper.insertTradeOrderDetail(tradeOrderDetail);
    }

    /**
     * 修改订单明细
     * 
     * @param tradeOrderDetail 订单明细
     * @return 结果
     */
    @Override
    public int updateTradeOrderDetail(TradeOrderDetail tradeOrderDetail)
    {
        tradeOrderDetail.setUpdateTime(DateUtils.getNowDate());
        return tradeOrderDetailMapper.updateTradeOrderDetail(tradeOrderDetail);
    }

    /**
     * 批量删除订单明细
     * 
     * @param ids 需要删除的订单明细ID
     * @return 结果
     */
    @Override
    public int deleteTradeOrderDetailByIds(Long[] ids)
    {
        return tradeOrderDetailMapper.deleteTradeOrderDetailByIds(ids);
    }

    /**
     * 删除订单明细信息
     * 
     * @param id 订单明细ID
     * @return 结果
     */
    @Override
    public int deleteTradeOrderDetailById(Long id)
    {
        return tradeOrderDetailMapper.deleteTradeOrderDetailById(id);
    }
}
