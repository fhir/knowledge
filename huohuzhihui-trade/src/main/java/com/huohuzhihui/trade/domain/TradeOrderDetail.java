package com.huohuzhihui.trade.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 订单明细对象 trade_order_detail
 * 
 * @author huohuzhihui
 * @date 2020-12-14
 */
public class TradeOrderDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 所属订单 */
    @Excel(name = "所属订单")
    private Long orderId;

    /** 商品类型：0课程1商品 */
    @Excel(name = "商品类型：0课程1商品")
    private Integer goodsType;

    /** 商品 */
    @Excel(name = "商品")
    private Long goodsId;

    /** 商品名称 */
    private String goodsName;

    /** 商品图片 */
    private String goodsImg;
    /** 数量 */
    @Excel(name = "数量")
    private Long quantity;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal amount;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 删除标示 */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setGoodsType(Integer goodsType) 
    {
        this.goodsType = goodsType;
    }

    public Integer getGoodsType() 
    {
        return goodsType;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setQuantity(Long quantity) 
    {
        this.quantity = quantity;
    }

    public Long getQuantity() 
    {
        return quantity;
    }
    public void setAmount(BigDecimal amount) 
    {
        this.amount = amount;
    }

    public BigDecimal getAmount() 
    {
        return amount;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("goodsType", getGoodsType())
            .append("goodsId", getGoodsId())
            .append("quantity", getQuantity())
            .append("amount", getAmount())
            .append("remarks", getRemarks())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
