import request from '@/utils/request'

// 查询账户变动，用于记录账户变动情况列表
export function listSituation(query) {
  return request({
    url: '/distribution/situation/list',
    method: 'get',
    params: query
  })
}

// 查询账户变动，用于记录账户变动情况详细
export function getSituation(id) {
  return request({
    url: '/distribution/situation/' + id,
    method: 'get'
  })
}

// 新增账户变动，用于记录账户变动情况
export function addSituation(data) {
  return request({
    url: '/distribution/situation',
    method: 'post',
    data: data
  })
}

// 修改账户变动，用于记录账户变动情况
export function updateSituation(data) {
  return request({
    url: '/distribution/situation',
    method: 'put',
    data: data
  })
}

// 删除账户变动，用于记录账户变动情况
export function delSituation(id) {
  return request({
    url: '/distribution/situation/' + id,
    method: 'delete'
  })
}

// 导出账户变动，用于记录账户变动情况
export function exportSituation(query) {
  return request({
    url: '/distribution/situation/export',
    method: 'get',
    params: query
  })
}