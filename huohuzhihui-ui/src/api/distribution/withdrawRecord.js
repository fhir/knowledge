import request from '@/utils/request'

// 查询提现列表
export function listWithdrawRecord(query) {
  return request({
    url: '/distribution/withdrawRecord/list',
    method: 'get',
    params: query
  })
}

// 查询提现详细
export function getWithdrawRecord(id) {
  return request({
    url: '/distribution/withdrawRecord/' + id,
    method: 'get'
  })
}

// 新增提现
export function addWithdrawRecord(data) {
  return request({
    url: '/distribution/withdrawRecord',
    method: 'post',
    data: data
  })
}

// 修改提现
export function updateWithdrawRecord(data) {
  return request({
    url: '/distribution/withdrawRecord',
    method: 'put',
    data: data
  })
}

// 删除提现
export function delWithdrawRecord(id) {
  return request({
    url: '/distribution/withdrawRecord/' + id,
    method: 'delete'
  })
}

// 导出提现
export function exportWithdrawRecord(query) {
  return request({
    url: '/distribution/withdrawRecord/export',
    method: 'get',
    params: query
  })
}