import request from '@/utils/request'

// 查询提现收费配置列表
export function listWithdrawParam(query) {
  return request({
    url: '/distribution/withdrawParam/list',
    method: 'get',
    params: query
  })
}

// 查询提现收费配置详细
export function getWithdrawParam(id) {
  return request({
    url: '/distribution/withdrawParam/' + id,
    method: 'get'
  })
}

// 新增提现收费配置
export function addWithdrawParam(data) {
  return request({
    url: '/distribution/withdrawParam',
    method: 'post',
    data: data
  })
}

// 修改提现收费配置
export function updateWithdrawParam(data) {
  return request({
    url: '/distribution/withdrawParam',
    method: 'put',
    data: data
  })
}

// 删除提现收费配置
export function delWithdrawParam(id) {
  return request({
    url: '/distribution/withdrawParam/' + id,
    method: 'delete'
  })
}

// 导出提现收费配置
export function exportWithdrawParam(query) {
  return request({
    url: '/distribution/withdrawParam/export',
    method: 'get',
    params: query
  })
}