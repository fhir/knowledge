import request from '@/utils/request'

// 查询分润记录列表
export function listRecord(query) {
  return request({
    url: '/distribution/record/list',
    method: 'get',
    params: query
  })
}

// 查询分润记录详细
export function getRecord(id) {
  return request({
    url: '/distribution/record/' + id,
    method: 'get'
  })
}

// 新增分润记录
export function addRecord(data) {
  return request({
    url: '/distribution/record',
    method: 'post',
    data: data
  })
}

// 修改分润记录
export function updateRecord(data) {
  return request({
    url: '/distribution/record',
    method: 'put',
    data: data
  })
}

// 删除分润记录
export function delRecord(id) {
  return request({
    url: '/distribution/record/' + id,
    method: 'delete'
  })
}

// 导出分润记录
export function exportRecord(query) {
  return request({
    url: '/distribution/record/export',
    method: 'get',
    params: query
  })
}