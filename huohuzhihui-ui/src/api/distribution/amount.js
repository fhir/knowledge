import request from '@/utils/request'

// 查询账户金额列表
export function listAmount(query) {
  return request({
    url: '/distribution/amount/list',
    method: 'get',
    params: query
  })
}

// 查询账户金额详细
export function getAmount(id) {
  return request({
    url: '/distribution/amount/' + id,
    method: 'get'
  })
}

// 新增账户金额
export function addAmount(data) {
  return request({
    url: '/distribution/amount',
    method: 'post',
    data: data
  })
}

// 修改账户金额
export function updateAmount(data) {
  return request({
    url: '/distribution/amount',
    method: 'put',
    data: data
  })
}

// 删除账户金额
export function delAmount(id) {
  return request({
    url: '/distribution/amount/' + id,
    method: 'delete'
  })
}

// 导出账户金额
export function exportAmount(query) {
  return request({
    url: '/distribution/amount/export',
    method: 'get',
    params: query
  })
}