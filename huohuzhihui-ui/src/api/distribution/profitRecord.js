import request from '@/utils/request'

// 查询分润记录列表
export function listProfitRecord(query) {
  return request({
    url: '/distribution/profitRecord/list',
    method: 'get',
    params: query
  })
}

// 查询分润记录详细
export function getProfitRecord(id) {
  return request({
    url: '/distribution/profitRecord/' + id,
    method: 'get'
  })
}

// 新增分润记录
export function addProfitRecord(data) {
  return request({
    url: '/distribution/profitRecord',
    method: 'post',
    data: data
  })
}

// 修改分润记录
export function updateProfitRecord(data) {
  return request({
    url: '/distribution/profitRecord',
    method: 'put',
    data: data
  })
}

// 删除分润记录
export function delProfitRecord(id) {
  return request({
    url: '/distribution/profitRecord/' + id,
    method: 'delete'
  })
}

// 导出分润记录
export function exportProfitRecord(query) {
  return request({
    url: '/distribution/profitRecord/export',
    method: 'get',
    params: query
  })
}