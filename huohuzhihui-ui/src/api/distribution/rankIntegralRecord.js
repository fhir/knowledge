import request from '@/utils/request'

// 查询系统积分记录列表
export function listRankIntegralRecord(query) {
  return request({
    url: '/distribution/rankIntegralRecord/list',
    method: 'get',
    params: query
  })
}

// 查询系统积分记录详细
export function getRankIntegralRecord(id) {
  return request({
    url: '/distribution/rankIntegralRecord/' + id,
    method: 'get'
  })
}

// 新增系统积分记录
export function addRankIntegralRecord(data) {
  return request({
    url: '/distribution/rankIntegralRecord',
    method: 'post',
    data: data
  })
}

// 修改系统积分记录
export function updateRankIntegralRecord(data) {
  return request({
    url: '/distribution/rankIntegralRecord',
    method: 'put',
    data: data
  })
}

// 删除系统积分记录
export function delRankIntegralRecord(id) {
  return request({
    url: '/distribution/rankIntegralRecord/' + id,
    method: 'delete'
  })
}

// 导出系统积分记录
export function exportRankIntegralRecord(query) {
  return request({
    url: '/distribution/rankIntegralRecord/export',
    method: 'get',
    params: query
  })
}