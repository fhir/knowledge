import request from '@/utils/request'

// 查询课程章节列表
export function listChapter(query) {
  return request({
    url: '/knowledge/chapter/list',
    method: 'get',
    params: query
  })
}

// 查询课程章节详细
export function getChapter(chapterId) {
  return request({
    url: '/knowledge/chapter/' + chapterId,
    method: 'get'
  })
}

// 新增课程章节
export function addChapter(data) {
  return request({
    url: '/knowledge/chapter',
    method: 'post',
    data: data
  })
}

// 修改课程章节
export function updateChapter(data) {
  return request({
    url: '/knowledge/chapter',
    method: 'put',
    data: data
  })
}

// 删除课程章节
export function delChapter(chapterId) {
  return request({
    url: '/knowledge/chapter/' + chapterId,
    method: 'delete'
  })
}

// 导出课程章节
export function exportChapter(query) {
  return request({
    url: '/knowledge/chapter/export',
    method: 'get',
    params: query
  })
}

// 查询课程
export function getAllCourse() {
  return request({
    url: '/knowledge/course/getAllCourse',
    method: 'get'
  })
}
