import request from '@/utils/request'

// 查询微信关注用户列表
export function listWeixinUser(query) {
  return request({
    url: '/weixin/weixinUser/list',
    method: 'get',
    params: query
  })
}

// 查询微信关注用户详细
export function getWeixinUser(id) {
  return request({
    url: '/weixin/weixinUser/' + id,
    method: 'get'
  })
}

// 新增微信关注用户
export function addWeixinUser(data) {
  return request({
    url: '/weixin/weixinUser',
    method: 'post',
    data: data
  })
}

// 修改微信关注用户
export function updateWeixinUser(data) {
  return request({
    url: '/weixin/weixinUser',
    method: 'put',
    data: data
  })
}

// 删除微信关注用户
export function delWeixinUser(id) {
  return request({
    url: '/weixin/weixinUser/' + id,
    method: 'delete'
  })
}

// 导出微信关注用户
export function exportWeixinUser(query) {
  return request({
    url: '/weixin/weixinUser/export',
    method: 'get',
    params: query
  })
}