import request from '@/utils/request'

// 查询公众号账号列表
export function listAccount(query) {
  return request({
    url: '/wxmp/account/list',
    method: 'get',
    params: query
  })
}

// 查询公众号账号详细
export function getAccount(appid) {
  return request({
    url: '/wxmp/account/' + appid,
    method: 'get'
  })
}

// 新增公众号账号
export function addAccount(data) {
  return request({
    url: '/wxmp/account',
    method: 'post',
    data: data
  })
}

// 修改公众号账号
export function updateAccount(data) {
  return request({
    url: '/wxmp/account',
    method: 'put',
    data: data
  })
}

// 删除公众号账号
export function delAccount(appid) {
  return request({
    url: '/wxmp/account/' + appid,
    method: 'delete'
  })
}

// 导出公众号账号
export function exportAccount(query) {
  return request({
    url: '/wxmp/account/export',
    method: 'get',
    params: query
  })
}

// 修改默认公众号账号
export function defaultAccount(appid) {
  return request({
    url: '/wxmp/account/setDefault/' + appid,
    method: 'get'    
  })
}