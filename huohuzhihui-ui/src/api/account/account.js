import request from '@/utils/request'

// 查询账户列表
export function listAccount(query) {
  return request({
    url: '/account/account/list',
    method: 'get',
    params: query
  })
}

// 查询账户详细
export function getAccount(id) {
  return request({
    url: '/account/account/' + id,
    method: 'get'
  })
}

// 新增账户
export function addAccount(data) {
  return request({
    url: '/account/account',
    method: 'post',
    data: data
  })
}

// 修改账户
export function updateAccount(data) {
  return request({
    url: '/account/account',
    method: 'put',
    data: data
  })
}

// 删除账户
export function delAccount(id) {
  return request({
    url: '/account/account/' + id,
    method: 'delete'
  })
}

// 导出账户
export function exportAccount(query) {
  return request({
    url: '/account/account/export',
    method: 'get',
    params: query
  })
}

//冻结账户
export function freezeAccount(ids) {
  var url =  '/account/account/freeze?';
  for(var i = 0 ; i <ids.length;i++ ){
      var id = ids[i];
      url = url+ "ids="+id + "&";
  }
  return request({
    url: url,
    method: 'post'
  })
}

//解冻账户
export function unfreezeAccount(ids) {
  var url =  '/account/account/unfreeze?';
  for(var i = 0 ; i <ids.length;i++ ){
    var id = ids[i];
    url = url+ "ids="+id + "&";
  }
  return request({
    url: url,
    method: 'post'
  })
}

//注销账户
export function cancelAccount(ids) {
  var url =  '/account/account/cancel?';
  for(var i = 0 ; i <ids.length;i++ ){
    var id = ids[i];
    url = url+ "ids="+id + "&";
  }
  return request({
    url: url,
    method: 'post'
  })
}

// 充值
export function recharge(id,amount,source,channelCode) {
  return request({
    url: '/account/account/recharge?id=' + id+'&amount='+amount+'&source='+source+'&channelCode='+channelCode,
    method: 'post'
  })
}
