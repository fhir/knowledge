package com.huohuzhihui.framework.message;

import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.context.ApplicationEvent;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.scheduling.annotation.Scheduled;

import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 使用redis 发布事件，支持分布式订阅
 * @author Administrator
 *
 */
public class EntityEventRedisPublisher implements IEntityEventPublisher {
    private final RedisTemplate<Object, Object > template;
    private final ChannelTopic topic;
    private final AtomicLong counter = new AtomicLong(0);

    public EntityEventRedisPublisher( 
    		final RedisTemplate<Object, Object> template,
            final ChannelTopic topic ) {
        this.template = template;
        this.topic = topic;
    }

    @Scheduled( fixedDelay = 100 )
    public void publish(ApplicationEvent entityEvent) {    	
        template.convertAndSend(topic.getTopic(), entityEvent);
    }
}
