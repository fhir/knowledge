package com.huohuzhihui.framework.message.event;

import org.springframework.context.ApplicationEvent;

import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.common.core.domain.entity.SysUser;

public class EntityUpdatedEvent extends ApplicationEvent {

	private BaseEntity entity;

	public EntityUpdatedEvent(Object source,BaseEntity entity) {
		super(source);
		this.entity = entity;
	}

	public BaseEntity getEntity() {
		return entity;
	}
}
