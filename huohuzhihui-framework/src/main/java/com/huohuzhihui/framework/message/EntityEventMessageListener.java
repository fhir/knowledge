package com.huohuzhihui.framework.message;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.context.ApplicationEvent;
import com.huohuzhihui.framework.message.event.*;

public class EntityEventMessageListener {
	Set<IEntityEventSubscriber> subscribers = new HashSet<>();
	Set<IUserEventSubscriber> userSubscribers = new HashSet<>();
    
    public void handleMessage(ApplicationEvent event,String channel) {
       
    	if(event instanceof UserRegisterEvent) {
    		UserRegisterEvent user = ((UserRegisterEvent) event);
	        for(IUserEventSubscriber subscriber: userSubscribers) {
	        	subscriber.onUserCreated(user);
	        }
        }
        else if(event instanceof UserUpdatedEvent) {
        	UserUpdatedEvent user = ((UserUpdatedEvent) event);
	        for(IUserEventSubscriber subscriber: userSubscribers) {
	        	subscriber.onUserUpdated(user);
	        }
        }
        else if(event instanceof UserDeletedEvent) {
        	UserDeletedEvent user = ((UserDeletedEvent) event);
	        for(IUserEventSubscriber subscriber: userSubscribers) {
	        	subscriber.onUserDeleted(user);
	        }
        }
        else if(event instanceof EntityCreatedEvent) {
        	EntityCreatedEvent entity = ((EntityCreatedEvent) event);
	        for(IEntityEventSubscriber subscriber: subscribers) {
	        	subscriber.onEntityCreated(entity);
	        }
        }
        else if(event instanceof EntityUpdatedEvent) {
        	EntityUpdatedEvent entity = ((EntityUpdatedEvent) event);
	        for(IEntityEventSubscriber subscriber: subscribers) {
	        	subscriber.onEntityUpdated(entity);
	        }
        }
        else if(event instanceof EntityDeletedEvent) {
        	EntityDeletedEvent entityId = ((EntityDeletedEvent) event);
	        for(IEntityEventSubscriber subscriber: subscribers) {
	        	subscriber.onEntityDeleted(entityId);
	        }
        }
    }
    
    public void subscrite(IEntityEventSubscriber sub) {
    	subscribers.add(sub);
    }
    
    public void unSubscrite(IEntityEventSubscriber sub) {
    	subscribers.remove(sub);
    }
    
    public void subscrite(IUserEventSubscriber sub) {
    	userSubscribers.add(sub);
    }
    
    public void unSubscrite(IUserEventSubscriber sub) {
    	userSubscribers.remove(sub);
    }
}

