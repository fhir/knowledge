package com.huohuzhihui.framework.message;

import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.scheduling.annotation.Scheduled;

import com.huohuzhihui.common.core.domain.BaseEntity;


/**
 * 使用spring contenx 发布事件。只能在jvm内部使用
 * @author Administrator
 *
 */
public class EntityEventSpringPublisher implements IEntityEventPublisher {
    private final ApplicationContext applicationContext;
    private final ChannelTopic topic;
    private final AtomicLong counter = new AtomicLong(0);

    public EntityEventSpringPublisher( 
    		final ApplicationContext template,
            final ChannelTopic topic ) {
        this.applicationContext = template;
        this.topic = topic;
    }

    @Scheduled( fixedDelay = 100 )
    public void publish(ApplicationEvent entityEvent) {
    	counter.incrementAndGet();    	
    	applicationContext.publishEvent(entityEvent);
    }
}
