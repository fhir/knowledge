package com.huohuzhihui.framework.message.event;

import org.springframework.context.ApplicationEvent;

import com.huohuzhihui.common.core.domain.entity.SysUser;

public class UserDeletedEvent extends ApplicationEvent {

	private SysUser user;

	public UserDeletedEvent(Object source,SysUser user) {
		super(source);
		this.user = user;
	}
	
	public SysUser getUser() {
		return user;
	}

}
