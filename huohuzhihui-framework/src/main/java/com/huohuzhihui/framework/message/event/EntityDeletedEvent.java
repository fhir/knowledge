package com.huohuzhihui.framework.message.event;

import org.springframework.context.ApplicationEvent;

import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.common.core.domain.entity.SysUser;

public class EntityDeletedEvent extends ApplicationEvent {

	private Object id;

	public EntityDeletedEvent(Object source,Object id) {
		super(source);
		this.id = id;
	}

	public Object getId() {
		return id;
	}
}
