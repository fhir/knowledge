package com.huohuzhihui.framework.message;

import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.framework.message.event.*;

public interface IUserEventSubscriber {
	
  default boolean onUserCreated(UserRegisterEvent event) {
	  return true;
  }
  
  default boolean onUserUpdated(UserUpdatedEvent event) {
	  return true;
  }
  
  default boolean onUserDeleted(UserDeletedEvent event) {
	  return true;
  }
}
