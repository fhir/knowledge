package com.huohuzhihui.framework.message;

import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.framework.message.event.*;

public interface IEntityEventSubscriber {
	
  default boolean onEntityCreated(EntityCreatedEvent event) {
	  return true;
  }
  
  default boolean onEntityUpdated(EntityUpdatedEvent event) {
	  return true;
  }
  
  default boolean onEntityDeleted(EntityDeletedEvent event) {
	  return true;
  }
}
