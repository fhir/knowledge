package com.huohuzhihui.framework.message;

import org.springframework.context.ApplicationEvent;

import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 发布实体变更事件
 * @author Administrator
 *
 */
public interface IEntityEventPublisher {
	
	void publish(ApplicationEvent entityEvent);

}
