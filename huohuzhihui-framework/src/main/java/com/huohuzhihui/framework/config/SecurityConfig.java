package com.huohuzhihui.framework.config;

import com.huohuzhihui.framework.security.device.DeviceAuthenticationFilter;
import com.huohuzhihui.framework.security.device.DeviceAuthenticationProvider;
import com.huohuzhihui.framework.security.mobile.MobileAuthenticationFilter;
import com.huohuzhihui.framework.security.mobile.MobileAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.web.filter.CorsFilter;
import com.huohuzhihui.framework.security.filter.JwtAuthenticationTokenFilter;
import com.huohuzhihui.framework.security.handle.AuthenticationEntryPointImpl;
import com.huohuzhihui.framework.security.handle.LogoutSuccessHandlerImpl;

/**
 * spring security配置
 * 
 * @author ruoyi
 */
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
    /**
     * 自定义用户认证逻辑
     */
    @Autowired
    @Qualifier("userDetailsServiceImpl")
    private UserDetailsService userDetailsService;

    @Autowired
    @Qualifier("mobileUserDetailsService")
    private UserDetailsService mobileUserDetailsService;

    @Autowired(required=false)
    @Qualifier("deviceUserDetailsService")
    private UserDetailsService deviceUserDetailsService;
    /**
     * 认证失败处理类
     */
    @Autowired
    private AuthenticationEntryPointImpl unauthorizedHandler;

    /**
     * 退出处理类
     */
    @Autowired
    private LogoutSuccessHandlerImpl logoutSuccessHandler;

    /**
     * token认证过滤器
     */
    @Autowired
    private JwtAuthenticationTokenFilter authenticationTokenFilter;

    /**
     * 跨域过滤器
     */
    @Autowired
    private CorsFilter corsFilter;

    @Autowired
    private MobileAuthenticationProvider mobileAuthenticationProvider;
    
    /**
     * 解决 无法直接注入 AuthenticationManager
     *
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception
    {
        return super.authenticationManagerBean();
    }

    /**
     * anyRequest          |   匹配所有请求路径
     * access              |   SpringEl表达式结果为true时可以访问
     * anonymous           |   匿名可以访问
     * denyAll             |   用户不能访问
     * fullyAuthenticated  |   用户完全认证可以访问（非remember-me下自动登录）
     * hasAnyAuthority     |   如果有参数，参数表示权限，则其中任何一个权限可以访问
     * hasAnyRole          |   如果有参数，参数表示角色，则其中任何一个角色可以访问
     * hasAuthority        |   如果有参数，参数表示权限，则其权限可以访问
     * hasIpAddress        |   如果有参数，参数表示IP地址，如果用户IP和参数匹配，则可以访问
     * hasRole             |   如果有参数，参数表示角色，则其角色可以访问
     * permitAll           |   用户可以任意访问
     * rememberMe          |   允许通过remember-me登录的用户访问
     * authenticated       |   用户登录后可访问
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception
    {
        httpSecurity
                // CSRF禁用，因为不使用session
                .csrf().disable()
                // 认证失败处理类
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                // 基于token，所以不需要session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                // 过滤请求
                .authorizeRequests()
                // 对于登录login 验证码captchaImage 允许匿名访问
                .antMatchers("/login", "/captchaImage").anonymous()
                .antMatchers(
                        HttpMethod.GET,
                        "/*.html",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js"
                ).permitAll()
                .antMatchers("/profile/**").anonymous()
                .antMatchers("/common/download**").anonymous()
                .antMatchers("/common/upload**").anonymous()
                .antMatchers("/common/download/resource**").anonymous()
                .antMatchers("/swagger-ui.html").anonymous()
                .antMatchers("/swagger-resources/**").anonymous()
                .antMatchers("/webjars/**").anonymous()
                .antMatchers("/*/api-docs").anonymous()
                .antMatchers("/druid/**").anonymous()
                .antMatchers("/api/wx/loginByMobile").anonymous()
                //获取微信openid
                .antMatchers("/api/wx/getUserInfo").anonymous()
                .antMatchers("/api/wx/getPhoneNumber").anonymous()
                //微信公众号token验证
                .antMatchers("/api/wx/token").anonymous()

                //微信支付通知
                .antMatchers("/api/pay/wxPayNotify").anonymous()
                //课程分类
                .antMatchers("/api/knowledge/findKnowledgeCategoryList").anonymous()
                //获取热门课程
                .antMatchers("/api/knowledge/findKnowledgeCourseList").anonymous()
                //登录
                .antMatchers("/api/user/login").anonymous()
                //刷新登录信息
                .antMatchers("/api/user/freshUser").anonymous()
                //获取课程详情
                .antMatchers("/api/knowledge/getCourseById").anonymous()
                //获取课程所有章节
                .antMatchers("/api/knowledge/findChapterListByCourseId").anonymous()
                //获取课程章节详情
                .antMatchers("/api/knowledge/getChapterByChapterId").anonymous()
                //提交订单
                .antMatchers("/api/pay/addOrder").anonymous()
                //公众号提交订单
                .antMatchers("/api/pay/webPay").anonymous()
                //公众号查询订单
                .antMatchers("/api/pay/queryOrder").anonymous()
                //更新用户信息
                .antMatchers("/api/user/updateUser").anonymous()
                //获取通知公告
                .antMatchers("/api/notice/findNoticeList").anonymous()
                .antMatchers("/api/wx/sendCode").anonymous()
                //获取订单
                .antMatchers("/api/order/findOrderListByUserId").anonymous()
                //查询收藏
                .antMatchers("/api/user/findCollectList").anonymous()
                //添加收藏
                .antMatchers("/api/user/addCollect").anonymous()
                //验证码
                .antMatchers("/api/sms/sendCode").anonymous()
                //注册
                .antMatchers("/api/user/register").anonymous()
                //微信菜单相关
                .antMatchers("/wx/menu/**").anonymous()
                //微信授权相关
                .antMatchers("/wx/redirect/**").anonymous()
                //微信JS-SDK相关
                .antMatchers("/api/wx/getWeixinJsConfig").anonymous()
                // 除上面外的所有请求全部需要鉴权认证
                .anyRequest().authenticated()
                .and()
                .headers().frameOptions().disable();
        /*------------------手机号登录过滤器开始--------------------- */
        MobileAuthenticationFilter mobileAuthenticationFilter = new MobileAuthenticationFilter();
        mobileAuthenticationFilter.setAuthenticationManager(httpSecurity.getSharedObject(AuthenticationManager.class));
        MobileAuthenticationProvider mobileAuthenticationProvider = new MobileAuthenticationProvider();
        mobileAuthenticationProvider.setUserDetailService(mobileUserDetailsService);
        httpSecurity.authenticationProvider(mobileAuthenticationProvider)
                .addFilterAfter(mobileAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        /*------------------手机号登录过滤器结束--------------------- */

        /*------------------设备号登录过滤器开始--------------------- */
        DeviceAuthenticationFilter deviceAuthenticationFilter = new DeviceAuthenticationFilter();
        deviceAuthenticationFilter.setAuthenticationManager(httpSecurity.getSharedObject(AuthenticationManager.class));
        DeviceAuthenticationProvider deviceAuthenticationProvider = new DeviceAuthenticationProvider();
        if(deviceUserDetailsService!=null) {
	        deviceAuthenticationProvider.setUserDetailService(deviceUserDetailsService);
	        httpSecurity.authenticationProvider(deviceAuthenticationProvider)
	                .addFilterAfter(deviceAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        }
        /*------------------设备号登录过滤器结束--------------------- */
        httpSecurity.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);
        // 添加JWT filter
        httpSecurity.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
        // 添加CORS filter
        httpSecurity.addFilterBefore(corsFilter, JwtAuthenticationTokenFilter.class);
        httpSecurity.addFilterBefore(corsFilter, LogoutFilter.class);
    }

    
    /**
     * 强散列哈希加密实现
     */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder()
    {
        return new BCryptPasswordEncoder();
    }

    /**
     * 身份认证接口
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
       // auth.authenticationProvider(mobileAuthenticationProvider);
    }
}
