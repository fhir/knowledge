package com.yiran.file.service;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.springframework.web.multipart.MultipartFile;



public interface IFileSystemService {
	
	

	/**
	 *	MultipartFile类型的文件上传ַ
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public String uploadFile(MultipartFile file) throws IOException;

	/**
	 * 普通的文件上传
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public String uploadFile(File file) throws IOException ;

	/**
	 * 带输入流形式的文件上传
	 * 
	 * @param is
	 * @param size
	 * @param fileName
	 * @return
	 */
	public String uploadFileStream(InputStream is, long size, String fileName);

	/**
	 * 将一段文本文件写到fastdfs的服务器上
	 * 
	 * @param content
	 * @param fileExtension
	 * @return
	 */
	public String uploadFile(String content, String fileExtension);

	/**
	 * 返回文件上传成功后的地址名称ַ
	 * @param storePath
	 * @return
	 */
	public String getResAccessUrl(String group,String path) ;

	/**
	 * 删除文件
	 * @param fileUrl
	 */
	public void deleteFile(String fileUrl);



}
