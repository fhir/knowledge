package com.yiran.file.domain;


import com.huohuzhihui.common.core.domain.BaseEntity;

import lombok.Data;

import java.util.Date;

/**
 * 文件表 sys_file_info
 * 
 * @author yiran
 * @date 2019-03-27
 */
@Data
public class FileInfo extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 编号 */
	private Integer id;
	/** 新名字 */
	private String newName;
	/** 原文件名 */
	private String oldName;
	/** 文件大小 */
	private Long size;
	/** 文件后缀 */
	private String suffix;
	/** 样式 */
	private String cssStyle;
	/** 类型  1文档2图片4音乐3视频5其它 */
	private String type;
	/** 文件存储空间名 */
	private String bucketName;
	/** OSS文件路径 */
	private String ossUrl;
	/** 文件夹名称 */
	private String fileName;
	/** 标签 */
	private String lable;
	

}
