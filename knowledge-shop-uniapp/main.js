/**
 * 知识店铺
 * 
 * .
 */
import Vue from 'vue'
import App from './App'
// #ifdef H5
import jwx from '@/request/jwx.js'
Vue.prototype.$jwx = jwx
// #endif
Vue.config.productionTip = false
App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()
