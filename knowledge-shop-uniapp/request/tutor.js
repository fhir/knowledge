/**
 * 知识店铺
 * 
 * .
 */
import request from '@/request/request.js'

// 获取讲师列表
export const getTutorList = () => {
	return request({
		url: 'index/teacher/index'
	})
}

// 获取讲师详情
export const postTutorDetail = (data) => {
	return request({
		url: 'index/teacher/detail',
		method: 'post',
		data
	})
}


// 讲师招募
export const postTeacherJsreg = (data) => {
	return request({
		url: 'index/teacher/js_reg',
		method: 'post',
		data
	})
}

// 发送验证码
export const postSendCode = (data) => {
	return request({
		url: 'index/teacher/sendcode',
		method: 'post',
		data
	})
}