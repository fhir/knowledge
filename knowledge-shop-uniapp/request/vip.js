/**
 * 知识店铺
 * 
 * .
 */
import request from '@/request/request.js'

export const getViptime = () => {
	return request({
		url: 'api/user/viptime'
	})
}

