/**
 * 知识店铺
 * 
 * .
 */
const BASE_URL = uni.BASE_URL
export const HOST_URL = uni.HOST_URL

console.log(BASE_URL)
function request(options) {
	return new Promise((resolve, reject) => {
		
		uni.request({
			url: BASE_URL + options.url,
			method: options.method || 'GET',
			data: options.data || {},
			header: options.header || {},
			success: res => {
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			},
			complete: () => {}
		});		
	
	})
}
export default request
