/**
 * 知识店铺
 * 
 * .
 */
import request from './request.js'

// 课程详情 details=详情
export const getCourseDetails = (data) => {
	return request({
		url: 'api/knowledge/getCourseById',
		method: 'POST',
		data
	})
}

 

// 我的课程
export const postMyCourse = (data) => {
	return request({
		url: 'api/order/findOrderListByUserId',
		method: 'post',
		data
	})
}


// 我的收藏
export const postMyDingyue = (data) => {
	return request({
		url: 'api/user/findCollectList',
		method: 'post',
		data
	})
}


// 课程评价
export const postCourseComment = (data) => {
	return request({
		url: 'index/courses/comment',
		method: 'post',
		data
	})
}
// 课程章节
export const findChapterListByCourseId = (data) => {
	return request({
		url: 'api/knowledge/findChapterListByCourseId',
		method: 'post',
		data
	})
}
//获取章节详情
export const getChapterByChapterId = (data) => {
	return request({
		url: 'api/knowledge/getChapterByChapterId',
		method: 'post',
		data
	})
}

