/**
 * 知识店铺
 * 
 * .
 */
import request from './request'


// 首页更多推荐课程
export const getMoretjCourse = () => {
	return request({
		url: 'api/knowledge/findKnowledgeCourseList'
	})
}
// 导航分类换一批
export const postDhHyp = (data) => {
	return request({
		url: 'api/knowledge/findKnowledgeCourseList',
		method: 'post',
		data
	})
}
// 分类列表接口
export const postClassifylist = (data) => {
	return request({
		url: 'api/knowledge/findKnowledgeCategoryChildren',
		method: 'post',
		data
	})
}

// 子分类列表接口
export const postSonflData = (data) => {
	return request({
		url: 'api/knowledge/findKnowledgeCategoryChildren',
		method: 'post',
		data
	})
}



// 课程接口
export const getIndexListInfo = (data) => {
	return request({
		url: 'api/knowledge/findKnowledgeCourseList',
		method: 'POST',
		data
	})
}


// 课程一级目录课程列表
export const postIndexGoods = (data) => {
	return request({
		url: 'api/knowledge/findKnowledgeCourseList',
		method: 'post',
		data
	})
}

// 课程目录
export const findKnowledgeCategoryList = (data) => {
	return request({
		url: 'api/knowledge/findKnowledgeCategoryList',
		method: 'post',
		data
	})
}
// 课程
export const findKnowledgeCourseList = (data) => {
	return request({
		url: 'api/knowledge/findKnowledgeCourseList',
		method: 'post',
		data
	})
}

