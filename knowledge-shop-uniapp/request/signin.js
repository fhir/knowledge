/**
 * 知识店铺
 * 
 * .
 */
import request from '@/request/request.js'

// 签到页
export const postDaka = (data) => {
	return request({
		url: 'index/user/daka',
		method: 'post',
		data
	})
}
export const ljqd = (data) => {
	return request({
		url: 'index/user/daka_jifen',
		method: 'post',
		data
	})
}