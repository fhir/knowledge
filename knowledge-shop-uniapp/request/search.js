/**
 * 知识店铺
 * 
 * .
 */
import request from './request'


// 首页
export const postSearchCourse = (data) => {
	return request({
		url: 'index/courses/search_course',
		method: 'post',
		data
	})
}