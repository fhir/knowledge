/**
 * 知识店铺
 * 
 * .
 */
import request from '@/request/request.js'


// 系统消息
export const postNewnotice = (data) => {
	data = data || {}
	data.noticeType = 2;
	data.status = 0;
	return request({
		url: 'api/notice/findNoticeList',
		method: 'post',
		data
	})
}

// 课程通知
export const findNoticeList = (data) => {
	data = data || {}
	data.status = 0;
	return request({
		url: 'api/notice/findNoticeList',
		method: 'post',
		data
	})
}