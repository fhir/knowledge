/**
 * 知识店铺
 * 
 * .
 */
const BASE_URL = uni.BASE_URL
const APP_ID = uni.APP_ID
export const checkUserinfo=()=> {
	
	
	const userinfo_update = uni.getStorageSync('userinfo');
	const pid=uni.getStorageSync('pid')

	if (!userinfo_update || !userinfo_update.uid) {
		setTimeout(function(){
			uni.navigateTo({
				url: '/pages/login/login',
			});
		},1500)
	}else{
		uni.request({
			url: BASE_URL+'api/user/freshUser',
			data: userinfo_update,
			method:'POST',
			success:(res) =>{
				let data=res.data.data
				userinfo_update.phone=data.user.phonenumber;
				userinfo_update.token=data.token;
				uni.setStorageSync('userinfo',userinfo_update)
				
				const userinfo_check = uni.getStorageSync('userinfo');
				//console.log(userinfo_check)
				
				if(!userinfo_check.phone || userinfo_check.phone==''){
					uni.navigateTo({
						url: '/pages/login/login?bindphone=1',
					});
				}
				/* else{
					uni.request({
						url: BASE_URL+'index/user/check_user',
						data: {
							uid:userinfo_check.uid,
							phone:userinfo_check.phone,
							token:userinfo_check.token,
						},
						method:'POST',
						success:(res) =>{
							// back(res)
							console.log(res.data)
							if(res.data.code==200){
								const userdata = res.data.data
								userinfo_check.userdata=userdata
								uni.setStorageSync('userinfo',userinfo_check)
							}else{
								uni.showToast({
									title: res.data.msg,
									duration: 2000,
									icon:'none'
								});
								uni.removeStorageSync('userinfo');
								setTimeout(function(){
									uni.navigateTo({
										url: '/pages/login/login',
									});
								},1500)
							}
						},
						fail:(err)=> {
				
						}
					});
				} */
			},
			fail:(err)=> {
		
			}
		});
	}
	}
	
	
	
	export const wx_login =()=>{
		var pid=uni.getStorageSync('pid')
		pid=pid?pid:0
		
		// #ifdef H5
		var ua = window.navigator.userAgent.toLowerCase();
		var appId = 'wx06f2d3b0846b0128';
		if (ua.match(/micromessenger/i) == 'micromessenger') {  
			   function GetQueryString(name) {
				 var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
				 var r = window.location.search.substr(1).match(reg); //获取url中"?"符后的字符串并正则匹配
				 var context = ""; 
				 if (r != null) 
				 context = r[2]; 
				 reg = null; 
				 r = null; 
				 return context == null || context == "" || context == "undefined" ? "" : context; 
			   }
		   
			   var code1 = GetQueryString('code')
			   localStorage.setItem('code',code1)
			   var code = localStorage.getItem('code')
			   if (code == null || code === '') {
				   var H5_URL =window.location.href.split('#')[0];
				   
				   
				   console.log(H5_URL);
				   //var H5_URL = "http://40bdd43.nat123.cc/";
				   //var H5_URL=uni.H5_URL
				window.location.href = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='+appId+
					'&redirect_uri=' + encodeURIComponent(H5_URL) + '&response_type=code&scope=snsapi_userinfo&state=#wechat_redirect'

			   }else{
				console.log(code);
				//查询openid及个人信息
				uni.request({
					url: BASE_URL+'wx/redirect/getOauthUserInfo',
					header: {
					  'content-type': 'application/x-www-form-urlencoded'
					},
					data: {
						appId:appId,
						code:code
					},
					method:'POST',
					success:(res) =>{
						console.log(res.data);
						var userinfo=uni.getStorageSync('userinfo');
						userinfo.openid=res.data.data.openid;
						uni.setStorageSync('userinfo',userinfo)
						
						var wxinfo = uni.getStorageSync('wxinfo');
						if(!wxinfo){
							wxinfo = {};
						}
						wxinfo.avatarUrl = res.data.data.headImgUrl;
						wxinfo.nickName=res.data.data.nickname;
						uni.setStorageSync('wxinfo',wxinfo);
						
						//绑定openid
						uni.request({
							url: BASE_URL+'api/user/updateUser',
							header: {
							  'content-type': 'application/x-www-form-urlencoded'
							},
							data: {
								uid:userinfo.uid?userinfo.uid:'',
								avatar:wxinfo.avatarUrl,
								openid:userinfo.openid,
								nickname:wxinfo.nickName,
							},
							method:'POST',
							success:(res) =>{
								console.log(res.data);
								var userinfo=uni.getStorageSync('userinfo');
								userinfo.uid=res.data.data.userId
								userinfo.phone=res.data.data.phonenumber
								userinfo.openid=res.data.data.openId;
								uni.setStorageSync('userinfo',userinfo)
								
								
								uni.switchTab({
									url:'/pages/user/user'
								})
							},
							fail:(res)=> {
								console.log(res.data);
							}
						});
						
						
					},
					fail:(res)=> {
						console.log(res.data);
					}
				});
				
				
				
				
		   }
		}
		// #endif
		// #ifdef MP-WEIXIN
			var apptype='MP-WEIXIN'
		const userinfo = uni.getStorageSync('userinfo');
		
		// 获取用户信息
		uni.getUserInfo({
		  provider: 'weixin',
		  success: function (infoRes) {
			uni.setStorageSync('wxinfo',infoRes.userInfo);
			uni.login({
			  provider: 'weixin',
			  success: function (loginRes) {
			    console.log(loginRes);
				const BASE_URL=uni.BASE_URL
				const code=loginRes.code
				uni.request({
					url: BASE_URL+'api/wx/getUserInfo',
					header: {
					  'content-type': 'application/x-www-form-urlencoded'
					},
					data: {
						code:code
					},
					method:'POST',
					success:(res) =>{
						console.log(res.data);
						let openid=res.data.data.openid
						var userinfo=uni.getStorageSync('userinfo');
						var wxinfo = uni.getStorageSync('wxinfo');
						uni.request({
							url: BASE_URL+'api/user/updateUser',
							header: {
							  'content-type': 'application/x-www-form-urlencoded'
							},
							data: {
								uid:userinfo.uid?userinfo.uid:'',
								avatar:wxinfo.avatarUrl,
								openid:openid,
								nickname:wxinfo.nickName,
							},
							method:'POST',
							success:(res) =>{
								console.log(res.data);
								var userinfo=uni.getStorageSync('userinfo');
								userinfo.uid=res.data.data.userId
								userinfo.phone=res.data.data.phonenumber
								userinfo.openid=res.data.data.openId;
								uni.setStorageSync('userinfo',userinfo)
								
								
								uni.switchTab({
									url:'/pages/user/user'
								})
							},
							fail:(res)=> {
								console.log(res.data);
							}
						});
					},
					fail:(res)=> {
						console.log(res.data);
					}
				});
			
			  }
			});
		  }
		});

		// #endif
		// #ifdef APP-PLUS
			var apptype='APP-PLUS'
			const userinfo = uni.getStorageSync('userinfo');
			uni.login({
			  provider: 'weixin',
			  success: function (loginRes) {
			    console.log(loginRes);
				const BASE_URL=uni.BASE_URL
				uni.request({
					url: BASE_URL+'index/user/app_wxauth',
					data: {
						token:loginRes.authResult.access_token,
						openid:loginRes.authResult.openid,
						apptype:apptype,
						pid:pid
					},
					method:'POST',
					success:(res) =>{
						console.log(res.data);
						var userinfo ={}
						userinfo.uid=res.data.data.uid
						userinfo.phone=res.data.data.phone?res.data.data.phone:''
						userinfo.token=res.data.data.token
						userinfo.userdata=res.data.data
						uni.setStorageSync('userinfo',userinfo)
						uni.switchTab({
							url:'/pages/user/user'
						})
					},
					fail:(res)=> {
						console.log(res.data);
					}
				});
			
			  }
			});
			
		// #endif
	}