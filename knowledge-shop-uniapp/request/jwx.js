/**
 * 知识店铺
 * 
 * .
 */
var jweixin = require('jweixin-module');  
var appId = 'wx06f2d3b0846b0128';
export default {
    //判断是否在微信中    
    isWechat: function() {  
			var ua = window.navigator.userAgent.toLowerCase();  
			if (ua.match(/micromessenger/i) == 'micromessenger') {  
				console.log('是微信客户端')  
				return true;  
			} else {  
				console.log('不是微信客户端')  
				return false;  
			}  
    },  
  initJssdk:function(callback){
  	var uri = window.location.href.split('#')[0];
  	console.log('nowurl'+uri)
  	const BASE_URL = uni.BASE_URL
  	uni.request({
  		url:BASE_URL+'api/wx/getWeixinJsConfig',
  		data:{
			appId:appId,
  			signUrl:uri  
  		},
		header: {
		  'content-type': 'application/x-www-form-urlencoded'
		},
  		method:'POST',
          success:(res)=>{
              jweixin.config({
                  debug: false,  
                  appId: res.data.data.appId,  
                  timestamp: res.data.data.timestamp,  
                  nonceStr: res.data.data.nonceStr,  
                  signature: res.data.data.signature,  
                  jsApiList: [//这里是需要用到的接口名称  
                      'checkJsApi',//判断当前客户端版本是否支持指定JS接口  
  					'updateTimelineShareData',
  					'updateAppMessageShareData',
  					'onMenuShareQQ',
  					'onMenuShareWeibo',
                      'getLocation',//获取位置  
                      'openLocation',//打开位置  
                      'scanQRCode',//扫一扫接口  
                      'chooseWXPay',//微信支付  
                      'chooseImage',//拍照或从手机相册中选图接口  
                      'previewImage',//预览图片接口  
                      'uploadImage'//上传图片  
                  ]  
              });  
              if (callback) {
                  callback(res.data);  
              }
          }
  	})
  },
    chooseImage:function(callback){//选择图片  
        if (!this.isWechat()) {  
					//console.log('不是微信客户端')  
					return;  
        }  
    },  
    //微信支付  
    wxpay: function(data,callback) {
        if (!this.isWechat()) {  
            //console.log('不是微信客户端')  
            return;  
        }  
		this.initJssdk(function(res) {
		    jweixin.ready(function() {  
				
		        jweixin.chooseWXPay({  
		            timestamp: data.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符  
		            nonceStr: data.nonceStr, // 支付签名随机串，不长于 32 位  
		            package: data.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=\*\*\*）  
		            signType: data.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'  
		            paySign: data.paySign, // 支付签名  
		            success: function (res) {  
		                callback(res) ;
						
		            },  
		            // 支付取消回调函数
					 cencel: function (res) {
					   uni.showToast({
					   	title: '用户取消支付~',
					   	icon: 'none'
					   });
					 },
					 // 支付失败回调函数
					 fail: function (res) {
					   uni.showToast({
					   	title: '支付失败~',
					   	icon: 'none'
					   });
					 }
		            // complete:function(res){  
		            //     console.log(res)  
		            // }  
		        });  
		    });  
		});  
    },
	// 分享
	share: function(data,callback) {
	    if (!this.isWechat()) {  
	        //console.log('不是微信客户端')  
	        return;  
	    }  
		
		this.initJssdk(function(res) {
		    jweixin.ready(function() {  
		        jweixin.updateAppMessageShareData({
					title: data.title, // 分享标题
					desc: data.desc, // 分享描述
					link: data.link, // 分享链接
					imgUrl: data.img_url, // 分享图标
					success: function (res) {
						callback(res)
					},
					cancel: function (res) {
					}
		        });
		    });  
		}); 
	    
	},
	sharepyq: function(data,callback) {
	    if (!this.isWechat()) {  
	        //console.log('不是微信客户端')  
	        return;  
	    }  
		
		this.initJssdk(function(res) {
		    jweixin.ready(function() {  
				jweixin.updateTimelineShareData({
				    title: data.title, // 分享标题
				    link: data.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
				    imgUrl: data.img_url, // 分享图标
				    success: function () {
				      callback(res)
				    },
					fail:function(err){
					}
				 })
		    });  
		});  
	    
	}
} 