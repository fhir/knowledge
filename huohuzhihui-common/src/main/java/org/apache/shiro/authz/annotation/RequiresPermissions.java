package org.apache.shiro.authz.annotation;

import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Target;

@Target(METHOD)
public @interface RequiresPermissions {
	String value();
}
