package com.huohuzhihui.knowledge.mapper;

import java.util.List;
import com.huohuzhihui.knowledge.domain.KnowledgeChapter;

/**
 * 课程章节Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
public interface KnowledgeChapterMapper 
{
    /**
     * 查询课程章节
     * 
     * @param chapterId 课程章节ID
     * @return 课程章节
     */
    public KnowledgeChapter selectKnowledgeChapterById(Long chapterId);

    /**
     * 查询课程章节列表
     * 
     * @param knowledgeChapter 课程章节
     * @return 课程章节集合
     */
    public List<KnowledgeChapter> selectKnowledgeChapterList(KnowledgeChapter knowledgeChapter);

    /**
     * 新增课程章节
     * 
     * @param knowledgeChapter 课程章节
     * @return 结果
     */
    public int insertKnowledgeChapter(KnowledgeChapter knowledgeChapter);

    /**
     * 修改课程章节
     * 
     * @param knowledgeChapter 课程章节
     * @return 结果
     */
    public int updateKnowledgeChapter(KnowledgeChapter knowledgeChapter);

    /**
     * 删除课程章节
     * 
     * @param chapterId 课程章节ID
     * @return 结果
     */
    public int deleteKnowledgeChapterById(Long chapterId);

    /**
     * 批量删除课程章节
     * 
     * @param chapterIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteKnowledgeChapterByIds(Long[] chapterIds);
}
