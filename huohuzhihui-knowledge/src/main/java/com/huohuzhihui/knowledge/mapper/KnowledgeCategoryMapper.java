package com.huohuzhihui.knowledge.mapper;

import java.util.List;
import com.huohuzhihui.knowledge.domain.KnowledgeCategory;

/**
 * 知识分类Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
public interface KnowledgeCategoryMapper 
{
    /**
     * 查询知识分类
     * 
     * @param categoryId 知识分类ID
     * @return 知识分类
     */
    public KnowledgeCategory selectKnowledgeCategoryById(Long categoryId);
    
    /**
     * 
     * @param categoryCode
     * @return
     */
    public KnowledgeCategory getKnowledgeCategoryByCode(String categoryCode);

    /**
     * 查询知识分类列表
     * 
     * @param knowledgeCategory 知识分类
     * @return 知识分类集合
     */
    public List<KnowledgeCategory> selectKnowledgeCategoryList(KnowledgeCategory knowledgeCategory);

    /**
     * 查询知识/课程列表 直接子列表
     * 
     * @param knowleageCourse 知识/课程
     * @return 知识/课程集合
     */
    public List<KnowledgeCategory> selectKnowledgeCategoryListChildren(Long categoryId);


    /**
     * 新增知识分类
     * 
     * @param knowledgeCategory 知识分类
     * @return 结果
     */
    public int insertKnowledgeCategory(KnowledgeCategory knowledgeCategory);

    /**
     * 修改知识分类
     * 
     * @param knowledgeCategory 知识分类
     * @return 结果
     */
    public int updateKnowledgeCategory(KnowledgeCategory knowledgeCategory);

    /**
     * 删除知识分类
     * 
     * @param categoryId 知识分类ID
     * @return 结果
     */
    public int deleteKnowledgeCategoryById(Long categoryId);

    /**
     * 批量删除知识分类
     * 
     * @param categoryIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteKnowledgeCategoryByIds(Long[] categoryIds);
}
