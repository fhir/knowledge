package com.huohuzhihui.knowledge.domain;

import com.huohuzhihui.common.core.domain.entity.SysMenu;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 知识分类对象 knowledge_category
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
public class KnowledgeCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long categoryId;
    
    /** 分类编码 */
    @Excel(name = "分类编码")
    private String categoryCode;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String categoryName;

    /** 父分类 */
    @Excel(name = "父分类")
    private Long parentCategoryId;

    /** 分类缩略图 */
    @Excel(name = "分类缩略图")
    private String categoryImg;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Long categorySort;

    /** 分类级别 */
    private Integer categoryLevel;

    /** 子级 */
    private List<KnowledgeCategory> children = new ArrayList<KnowledgeCategory>();
    
    public Long getId() 
    {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }
    
    public void setCategoryCode(String categoryCode) 
    {
        this.categoryCode = categoryCode;
    }

    public String getCategoryCode() 
    {
        return categoryCode;
    }
    
    public void setCategoryName(String categoryName) 
    {
        this.categoryName = categoryName;
    }

    public String getCategoryName() 
    {
        return categoryName;
    }
    public void setParentCategoryId(Long parentCategoryId) 
    {
        this.parentCategoryId = parentCategoryId;
    }

    public Long getParentCategoryId() 
    {
        return parentCategoryId;
    }
    public void setCategoryImg(String categoryImg) 
    {
        this.categoryImg = categoryImg;
    }

    public String getCategoryImg() 
    {
        return categoryImg;
    }
    public void setCategorySort(Long categorySort) 
    {
        this.categorySort = categorySort;
    }

    public Long getCategorySort() 
    {
        return categorySort;
    }
    public void setCategoryLevel(Integer categoryLevel) 
    {
        this.categoryLevel = categoryLevel;
    }

    public Integer getCategoryLevel() 
    {
        return categoryLevel;
    }

    public List<KnowledgeCategory> getChildren() {
        return children;
    }

    public void setChildren(List<KnowledgeCategory> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("categoryId", getCategoryId())
            .append("categoryName", getCategoryName())
            .append("parentCategoryId", getParentCategoryId())
            .append("categoryImg", getCategoryImg())
            .append("categorySort", getCategorySort())
            .append("categoryLevel", getCategoryLevel())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
