package com.huohuzhihui.knowledge.service;

import java.util.List;
import com.huohuzhihui.knowledge.domain.KnowleageCourse;

/**
 * 知识/课程Service接口
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
public interface IKnowleageCourseService 
{
    /**
     * 查询知识/课程
     * 
     * @param courseId 知识/课程ID
     * @return 知识/课程
     */
    public KnowleageCourse selectKnowleageCourseById(Long courseId);

    /**
     * 查询知识/课程列表
     * 
     * @param knowleageCourse 知识/课程
     * @return 知识/课程集合
     */
    public List<KnowleageCourse> selectKnowleageCourseList(KnowleageCourse knowleageCourse);

    /**
     * 新增知识/课程
     * 
     * @param knowleageCourse 知识/课程
     * @return 结果
     */
    public int insertKnowleageCourse(KnowleageCourse knowleageCourse);

    /**
     * 修改知识/课程
     * 
     * @param knowleageCourse 知识/课程
     * @return 结果
     */
    public int updateKnowleageCourse(KnowleageCourse knowleageCourse);

    /**
     * 批量删除知识/课程
     * 
     * @param courseIds 需要删除的知识/课程ID
     * @return 结果
     */
    public int deleteKnowleageCourseByIds(Long[] courseIds);

    /**
     * 删除知识/课程信息
     * 
     * @param courseId 知识/课程ID
     * @return 结果
     */
    public int deleteKnowleageCourseById(Long courseId);
}
