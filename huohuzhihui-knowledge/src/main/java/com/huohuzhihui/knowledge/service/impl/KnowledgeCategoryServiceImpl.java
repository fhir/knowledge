package com.huohuzhihui.knowledge.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.huohuzhihui.common.core.domain.TreeSelect;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.knowledge.mapper.KnowledgeCategoryMapper;
import com.huohuzhihui.knowledge.domain.KnowledgeCategory;
import com.huohuzhihui.knowledge.service.IKnowledgeCategoryService;

/**
 * 知识分类Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
@Service
public class KnowledgeCategoryServiceImpl implements IKnowledgeCategoryService 
{
    @Autowired
    private KnowledgeCategoryMapper knowledgeCategoryMapper;

    /**
     * 查询知识分类
     * 
     * @param categoryId 知识分类ID
     * @return 知识分类
     */
    @Override
    public KnowledgeCategory selectKnowledgeCategoryById(Long categoryId)
    {
        return knowledgeCategoryMapper.selectKnowledgeCategoryById(categoryId);
    }

    /**
     * 查询知识分类列表
     * 
     * @param knowledgeCategory 知识分类
     * @return 知识分类
     */
    @Override
    public List<KnowledgeCategory> selectKnowledgeCategoryList(KnowledgeCategory knowledgeCategory)
    {
        return knowledgeCategoryMapper.selectKnowledgeCategoryList(knowledgeCategory);
    }

    /**
     * 新增知识分类
     * 
     * @param knowledgeCategory 知识分类
     * @return 结果
     */
    @Override
    public int insertKnowledgeCategory(KnowledgeCategory knowledgeCategory)
    {
        knowledgeCategory.setCreateTime(DateUtils.getNowDate());
        return knowledgeCategoryMapper.insertKnowledgeCategory(knowledgeCategory);
    }

    /**
     * 修改知识分类
     * 
     * @param knowledgeCategory 知识分类
     * @return 结果
     */
    @Override
    public int updateKnowledgeCategory(KnowledgeCategory knowledgeCategory)
    {
        knowledgeCategory.setUpdateTime(DateUtils.getNowDate());
        return knowledgeCategoryMapper.updateKnowledgeCategory(knowledgeCategory);
    }

    /**
     * 批量删除知识分类
     * 
     * @param categoryIds 需要删除的知识分类ID
     * @return 结果
     */
    @Override
    public int deleteKnowledgeCategoryByIds(Long[] categoryIds)
    {
        return knowledgeCategoryMapper.deleteKnowledgeCategoryByIds(categoryIds);
    }

    /**
     * 删除知识分类信息
     * 
     * @param categoryId 知识分类ID
     * @return 结果
     */
    @Override
    public int deleteKnowledgeCategoryById(Long categoryId)
    {
        return knowledgeCategoryMapper.deleteKnowledgeCategoryById(categoryId);
    }

	@Override
	public KnowledgeCategory getKnowledgeCategoryByCode(String categoryCode) {
		// TODO Auto-generated method stub
		return knowledgeCategoryMapper.getKnowledgeCategoryByCode(categoryCode);
	}

	@Override
	public List<TreeSelect> buildCategoryTreeSelect(List<KnowledgeCategory> terms) {
		List<TreeSelect> root = new ArrayList<TreeSelect>();
		Map<Long,TreeSelect> tree = new HashMap<Long,TreeSelect>();		
		for(KnowledgeCategory c: terms) {
			if(c.getParentCategoryId()==null) {
				TreeSelect ts = tree.get(c.getCategoryId());
				if(ts==null) {
					ts = new TreeSelect();
					ts.setId(c.getCategoryId());
					ts.setLabel(c.getCategoryName());
					tree.put(c.getCategoryId(), ts);	
					root.add(ts);					
				}
			}
		}
		
		for(KnowledgeCategory c: terms) {
			if(c.getParentCategoryId()!=null) {
				TreeSelect ts = tree.get(c.getParentCategoryId());
				if(ts!=null && !tree.containsKey(c.getCategoryId())) {
					TreeSelect ts2 = new TreeSelect();
					ts2.setId(c.getCategoryId());
					ts2.setLabel(c.getCategoryName());
					ts.getOrCreateChildren().add(ts2);
					tree.put(c.getCategoryId(), ts2);
				}
			}
		}
		
		for(KnowledgeCategory c: terms) {
			if(c.getParentCategoryId()!=null) {
				TreeSelect ts = tree.get(c.getParentCategoryId());
				if(ts!=null && !tree.containsKey(c.getCategoryId())) {
					TreeSelect ts2 = new TreeSelect();
					ts2.setId(c.getCategoryId());
					ts2.setLabel(c.getCategoryName());
					ts.getOrCreateChildren().add(ts2);
					tree.put(c.getCategoryId(), ts2);
				}
			}
		}
		
		return root;
	}

	@Override
	public List<KnowledgeCategory> selectKnowledgeCategoryListChildren(Long categoryId) {
		// TODO Auto-generated method stub
		return knowledgeCategoryMapper.selectKnowledgeCategoryListChildren(categoryId);
	}
}
