package com.huohuzhihui.knowledge.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.knowledge.mapper.KnowledgeChapterMapper;
import com.huohuzhihui.knowledge.domain.KnowledgeChapter;
import com.huohuzhihui.knowledge.service.IKnowledgeChapterService;

/**
 * 课程章节Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
@Service
public class KnowledgeChapterServiceImpl implements IKnowledgeChapterService 
{
    @Autowired
    private KnowledgeChapterMapper knowledgeChapterMapper;

    /**
     * 查询课程章节
     * 
     * @param chapterId 课程章节ID
     * @return 课程章节
     */
    @Override
    public KnowledgeChapter selectKnowledgeChapterById(Long chapterId)
    {
        return knowledgeChapterMapper.selectKnowledgeChapterById(chapterId);
    }

    /**
     * 查询课程章节列表
     * 
     * @param knowledgeChapter 课程章节
     * @return 课程章节
     */
    @Override
    public List<KnowledgeChapter> selectKnowledgeChapterList(KnowledgeChapter knowledgeChapter)
    {
        return knowledgeChapterMapper.selectKnowledgeChapterList(knowledgeChapter);
    }

    /**
     * 新增课程章节
     * 
     * @param knowledgeChapter 课程章节
     * @return 结果
     */
    @Override
    public int insertKnowledgeChapter(KnowledgeChapter knowledgeChapter)
    {
        knowledgeChapter.setCreateTime(DateUtils.getNowDate());
        return knowledgeChapterMapper.insertKnowledgeChapter(knowledgeChapter);
    }

    /**
     * 修改课程章节
     * 
     * @param knowledgeChapter 课程章节
     * @return 结果
     */
    @Override
    public int updateKnowledgeChapter(KnowledgeChapter knowledgeChapter)
    {
        knowledgeChapter.setUpdateTime(DateUtils.getNowDate());
        return knowledgeChapterMapper.updateKnowledgeChapter(knowledgeChapter);
    }

    /**
     * 批量删除课程章节
     * 
     * @param chapterIds 需要删除的课程章节ID
     * @return 结果
     */
    @Override
    public int deleteKnowledgeChapterByIds(Long[] chapterIds)
    {
        return knowledgeChapterMapper.deleteKnowledgeChapterByIds(chapterIds);
    }

    /**
     * 删除课程章节信息
     * 
     * @param chapterId 课程章节ID
     * @return 结果
     */
    @Override
    public int deleteKnowledgeChapterById(Long chapterId)
    {
        return knowledgeChapterMapper.deleteKnowledgeChapterById(chapterId);
    }
}
