package com.huohuzhihui.web.controller.knowledge;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.knowledge.domain.KnowledgeChapter;
import com.huohuzhihui.knowledge.service.IKnowledgeChapterService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 课程章节Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
@RestController
@RequestMapping("/knowledge/chapter")
public class KnowledgeChapterController extends BaseController
{
    @Autowired
    private IKnowledgeChapterService knowledgeChapterService;

    /**
     * 查询课程章节列表
     */
    @PreAuthorize("@ss.hasPermi('knowledge:chapter:list')")
    @GetMapping("/list")
    public TableDataInfo list(KnowledgeChapter knowledgeChapter)
    {
        startPage();
        List<KnowledgeChapter> list = knowledgeChapterService.selectKnowledgeChapterList(knowledgeChapter);
        return getDataTable(list);
    }

    /**
     * 导出课程章节列表
     */
    @PreAuthorize("@ss.hasPermi('knowledge:chapter:export')")
    @Log(title = "课程章节", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(KnowledgeChapter knowledgeChapter)
    {
        List<KnowledgeChapter> list = knowledgeChapterService.selectKnowledgeChapterList(knowledgeChapter);
        ExcelUtil<KnowledgeChapter> util = new ExcelUtil<KnowledgeChapter>(KnowledgeChapter.class);
        return util.exportExcel(list, "chapter");
    }

    /**
     * 获取课程章节详细信息
     */
    @PreAuthorize("@ss.hasPermi('knowledge:chapter:query')")
    @GetMapping(value = "/{chapterId}")
    public AjaxResult getInfo(@PathVariable("chapterId") Long chapterId)
    {
        return AjaxResult.success(knowledgeChapterService.selectKnowledgeChapterById(chapterId));
    }

    /**
     * 新增课程章节
     */
    @PreAuthorize("@ss.hasPermi('knowledge:chapter:add')")
    @Log(title = "课程章节", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody KnowledgeChapter knowledgeChapter)
    {
        return toAjax(knowledgeChapterService.insertKnowledgeChapter(knowledgeChapter));
    }

    /**
     * 修改课程章节
     */
    @PreAuthorize("@ss.hasPermi('knowledge:chapter:edit')")
    @Log(title = "课程章节", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody KnowledgeChapter knowledgeChapter)
    {
        return toAjax(knowledgeChapterService.updateKnowledgeChapter(knowledgeChapter));
    }

    /**
     * 删除课程章节
     */
    @PreAuthorize("@ss.hasPermi('knowledge:chapter:remove')")
    @Log(title = "课程章节", businessType = BusinessType.DELETE)
	@DeleteMapping("/{chapterIds}")
    public AjaxResult remove(@PathVariable Long[] chapterIds)
    {
        return toAjax(knowledgeChapterService.deleteKnowledgeChapterByIds(chapterIds));
    }
}
