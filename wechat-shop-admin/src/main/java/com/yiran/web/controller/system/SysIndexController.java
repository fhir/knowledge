package com.yiran.web.controller.system;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.huohuzhihui.common.config.RuoYiConfig;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.entity.SysMenu;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.utils.file.FileUtils;
import com.huohuzhihui.framework.web.domain.server.Jvm;
import com.huohuzhihui.system.service.ISysMenuService;
import com.yiran.common.config.Global;
import com.yiran.framework.util.ShiroUtils;

/**
 * 首页 业务处理
 * 
 * @author yiran
 */
@Controller
public class SysIndexController extends BaseController
{
    @Autowired
    private ISysMenuService menuService;
    
    public SysUser getSysUser()
    {
        return ShiroUtils.getSysUser();
    }

    public void setSysUser(SysUser user)
    {
        ShiroUtils.setSysUser(user);
    }

    public Long getUserId()
    {
        return getSysUser().getUserId();
    }

    public String getLoginName()
    {
        return getSysUser().getUserName();
    }
    

    // 系统首页
    @GetMapping("/index")
    public String index(ModelMap mmap)
    {
        // 取身份信息
        SysUser user = getSysUser();
        // 根据用户id取出菜单
        List<SysMenu> menus = menuService.selectMenuList(user.getUserId());
        List<SysMenu> menusTree = menuService.buildMenuTree(menus);
        mmap.put("menus", menusTree);
        mmap.put("user", user);
        mmap.put("copyrightYear", Global.getCopyrightYear());
        mmap.put("demoEnabled", Global.isDemoEnabled());
        return "index";
    }

    
    // 系统介绍
    @GetMapping("/system/main")
    public String main(ModelMap mmap)
    {
    	Jvm jvm = new Jvm();//虚拟机信息
        Properties props = System.getProperties();
        jvm.setTotal(Runtime.getRuntime().totalMemory());
        jvm.setMax(Runtime.getRuntime().maxMemory());
        jvm.setFree(Runtime.getRuntime().freeMemory());
        jvm.setVersion(props.getProperty("java.version"));
        jvm.setHome(props.getProperty("java.home"));
        mmap.put("jvm",jvm);
        return "main";
    }
    
    // 系统介绍
    @GetMapping("/profile/avatar/**")
    public void avatar(HttpServletRequest request,HttpServletResponse response) throws IOException
    {
    	String file = request.getRequestURI();
    	file = file.replace("/profile/avatar/", "");
    	if(file.startsWith("http://") || file.startsWith("https://")) {
    		response.sendRedirect(file);
    	}
    	else {
    		File filePath = new File(RuoYiConfig.getAvatarPath(),file);
    		response.setContentType("image/png");
    		FileUtils.writeBytes(filePath.getPath(), response.getOutputStream());
    	}
    }
}
