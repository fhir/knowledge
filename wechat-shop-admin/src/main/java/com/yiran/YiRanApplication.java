package com.yiran;

import javax.annotation.Resource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;

import com.huohuzhihui.framework.web.service.PermissionService;

/**
 * 启动程序
 * @author yiran
 */

@SpringBootApplication(
		exclude = { DataSourceAutoConfiguration.class,SecurityAutoConfiguration.class  },
		scanBasePackages = {"com.huohuzhihui","com.yiran"})
public class YiRanApplication
{
	@Resource(name="ss")
	private PermissionService permission;
	
    public static void main(String[] args)
    {
        SpringApplication.run(YiRanApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  微信商城平台启动成功   ლ(´ڡ`ლ)");
    }
    

    @Bean("permission") // alias ss
    public PermissionService permissionService() {
    	return permission;
    }
}