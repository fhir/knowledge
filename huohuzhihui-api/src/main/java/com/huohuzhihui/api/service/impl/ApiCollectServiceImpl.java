package com.huohuzhihui.api.service.impl;

import com.huohuzhihui.account.domain.AccCollect;
import com.huohuzhihui.account.mapper.AccCollectMapper;
import com.huohuzhihui.api.domain.ApiCollect;
import com.huohuzhihui.api.domain.ApiOrder;
import com.huohuzhihui.api.service.ApiCollectService;
import com.huohuzhihui.api.service.ApiOrderService;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.knowledge.domain.KnowleageCourse;
import com.huohuzhihui.knowledge.mapper.KnowleageCourseMapper;
import com.huohuzhihui.trade.domain.TradeOrder;
import com.huohuzhihui.trade.domain.TradeOrderDetail;
import com.huohuzhihui.trade.mapper.TradeOrderDetailMapper;
import com.huohuzhihui.trade.mapper.TradeOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订单Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-11-15
 */
@Service
public class ApiCollectServiceImpl implements ApiCollectService {
    @Autowired
    private AccCollectMapper accCollectMapper;
    @Autowired
    private KnowleageCourseMapper knowleageCourseMapper;




    @Override
    public int addCollect(ApiCollect apiCollect) {
        AccCollect accCollect = new AccCollect();
        accCollect.setSourceId(apiCollect.getSourceId());
        accCollect.setType(apiCollect.getType());
        accCollect.setStatus(1);
        accCollect.setUserId(apiCollect.getUserId());
        accCollect.setCreateTime(DateUtils.getNowDate());

        return this.accCollectMapper.insertAccCollect(accCollect);
    }

    @Override
    public List<ApiCollect> findCollectList(ApiCollect apiCollect) {
        List<ApiCollect> apiCollectList = new ArrayList<ApiCollect>();

        AccCollect accCollect = new AccCollect();
        accCollect.setUserId(apiCollect.getUserId());
        accCollect.setType(apiCollect.getType());

        List<AccCollect> accCollectList = this.accCollectMapper.selectAccCollectList(accCollect);
        if(accCollectList!=null && accCollectList.size()>0){
            for(int i = 0; i < accCollectList.size(); i++){
                AccCollect collect = accCollectList.get(i);

                ApiCollect returnApiCollect = new ApiCollect();
                returnApiCollect.setSourceId(collect.getSourceId());
                returnApiCollect.setType(collect.getType());

                KnowleageCourse knowleageCourse = this.knowleageCourseMapper.selectKnowleageCourseById(collect.getSourceId());
                returnApiCollect.setKnowleageCourse(knowleageCourse);

                apiCollectList.add(returnApiCollect);
            }
        }
        return apiCollectList;
    }


    @Override
    public AccCollect findCollectByUserIdAndSourceId(Long userId,Long sourceId) {

        AccCollect accCollect = new AccCollect();
        accCollect.setUserId(userId);
        accCollect.setSourceId(sourceId);

        List<AccCollect> accCollectList = this.accCollectMapper.selectAccCollectList(accCollect);

        if(accCollectList!=null && accCollectList.size()>0){
            AccCollect collect = accCollectList.get(0);

            return collect;
        }
        return null;
    }

    @Override
    public int deleteCollect(Long id) {
        return this.accCollectMapper.deleteAccCollectById(id);
    }
}

