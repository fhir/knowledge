package com.huohuzhihui.api.service;

import com.huohuzhihui.api.domain.ApiOrder;
import com.huohuzhihui.trade.domain.TradeOrderDetail;

import java.util.List;

/**
 * 通知公告Service接口
 * 
 * @author huohuzhihui
 * @date 2020-11-15
 */
public interface ApiOrderService
{
    /**
     * 查询买到的课程
     * @param userId
     * @return
     */
    public List<TradeOrderDetail> findOrderListByUserId(Long userId);

}
