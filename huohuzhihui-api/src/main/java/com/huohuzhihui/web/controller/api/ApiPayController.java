package com.huohuzhihui.web.controller.api;

import com.huohuzhihui.account.domain.AccUserAccount;
import com.huohuzhihui.api.service.ApiAccountService;
import com.huohuzhihui.api.service.ApiPayService;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.utils.StringUtils;
import com.ijpay.core.kit.IpKit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

/**
 * 商户Controller
 * 
 * @author zylu
 * @date 2020-11-14
 */
@RestController
@RequestMapping("/api/pay")
public class ApiPayController extends BaseController
{
    @Autowired
    private ApiAccountService apiAccountService;

    @Autowired
    private ApiPayService apiPayService;

    @PostMapping("/addOrder")
    @ResponseBody
    public AjaxResult addOrder(HttpServletRequest request, Long userId,String openId,Long courseId,BigDecimal amount, String source, String channelCode){
        SysUser sysUser = apiAccountService.getUserById(userId);
        AccUserAccount accUserAccount = apiAccountService.getAccountByUserId(sysUser.getUserId());
        accUserAccount.setUserName(sysUser.getUserName());
        String ip = IpKit.getRealIp(request);
        if (StringUtils.isBlank(ip)) {
            ip = "127.0.0.1";
        }
        return new AjaxResult(200, "获取支付参数成功", apiPayService.addOrder(   sysUser, amount,  source, channelCode,openId,ip,courseId));
    }

    /**
     * 异步通知
     */
    @RequestMapping(value = "/wxPayNotify", method = {RequestMethod.POST, RequestMethod.GET})
    public String wxPayNotify(HttpServletRequest request) {
        return  apiAccountService.wxPayNotify(request  );
    }



    /**
     * 微信H5 支付
     * 注意：必须再web页面中发起支付且域名已添加到开发配置中
     */
    @RequestMapping(value = "/webPay", method = {RequestMethod.POST, RequestMethod.GET})
    public AjaxResult webPay(HttpServletRequest request, Long userId,String openid,Long courseId,String amount, String source, String channelCode)  {
        String ip = IpKit.getRealIp(request);
        if (StringUtils.isBlank(ip)) {
            ip = "127.0.0.1";
        }
        if (StringUtils.isEmpty(openid)) {
            return AjaxResult.error("openId is null");
        }
        if (StringUtils.isEmpty(amount)) {
            return AjaxResult.error("请输入数字金额");
        }

        try{
            String result = apiPayService.webPay(   userId, amount,  source, channelCode,openid,ip,courseId);
            return new AjaxResult(200, "获取支付参数成功", result);
        }catch (Exception e){
            return AjaxResult.error(e.getMessage());
        }
    }


    @RequestMapping(value = "/queryOrder", method = {RequestMethod.POST})
    public AjaxResult queryOrder(String outTradeNo)  {
        try{
            String result = apiPayService.queryOrder(outTradeNo);
            return new AjaxResult(200, "查询订单成功", result);
        }catch (Exception e){
            return AjaxResult.error(e.getMessage());
        }
    }


}
