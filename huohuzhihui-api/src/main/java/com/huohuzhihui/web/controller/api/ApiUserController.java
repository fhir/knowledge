package com.huohuzhihui.web.controller.api;

import com.huohuzhihui.account.domain.AccCollect;
import com.huohuzhihui.account.domain.AccUserAccount;
import com.huohuzhihui.account.service.IAccUserAccountService;
import com.huohuzhihui.api.domain.ApiCollect;
import com.huohuzhihui.api.domain.ApiUser;
import com.huohuzhihui.api.service.ApiAccountService;
import com.huohuzhihui.api.service.ApiCollectService;
import com.huohuzhihui.api.service.ApiKnowledgeService;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.constant.UserConstants;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.core.domain.model.LoginUser;
import com.huohuzhihui.common.core.redis.RedisCache;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.framework.web.service.TokenService;
import com.huohuzhihui.knowledge.domain.KnowleageCourse;
import com.huohuzhihui.knowledge.domain.KnowledgeCategory;
import com.huohuzhihui.system.mapper.SysUserMapper;
import com.huohuzhihui.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * 商户Controller
 * 
 * @author zylu
 * @date 2020-11-14
 */
@RestController
@RequestMapping("/api/user")
public class ApiUserController extends BaseController
{
    @Autowired
    private ApiAccountService apiAccountService;
    @Autowired
    private ApiCollectService apiCollectService;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private RedisCache redisCache;
    
    @Autowired
    private IAccUserAccountService iAccUserAccountService;
    
    @Autowired
    private TokenService tokenService;


    @PostMapping("/login")
    public AjaxResult login(@RequestBody SysUser user){
        LoginUser loginUser = apiAccountService.login(user.getPhonenumber(),user.getPassword());
        return AjaxResult.success(loginUser);
    }


    @PostMapping("/freshUser")
    public AjaxResult freshUser(@RequestBody LoginUser user){
        LoginUser loginUser  = apiAccountService.freshUser(user);
        return AjaxResult.success(loginUser);
    }

    @PostMapping("/updateUser")
    public AjaxResult updateUser(Long uid,String avatar,String openid,String nickname){
        SysUser user = apiAccountService.getUserById(uid);
        user.setAvatar(avatar);
        user.setOpenId(openid);
        user.setNickName(nickname);

        return AjaxResult.success(this.apiAccountService.updateUser(user));
    }

    /**
     * 收藏
     */
    @PostMapping("/addCollect")
    public AjaxResult addCollect(@RequestBody ApiCollect apiCollect)
    {
        AccCollect collect = apiCollectService.findCollectByUserIdAndSourceId(apiCollect.getUserId(),apiCollect.getSourceId());
        if(collect!=null){
            apiCollectService.deleteCollect(collect.getId());
            return AjaxResult.success();
        }
        return AjaxResult.success(apiCollectService.addCollect(apiCollect));
    }
    
    @PostMapping("/findCollectList")
    public AjaxResult findCollectList(@RequestBody ApiCollect apiCollect){

        return AjaxResult.success(apiCollectService.findCollectList(apiCollect));
    }

    @PostMapping("/register")
    public AjaxResult register( SysUser user,String code){
        //判断验证码是否正确
        String verificationCode = redisCache.getCacheObject(user.getPhonenumber());
        if(!verificationCode.equalsIgnoreCase(code)){
            return AjaxResult.error("注册失败，验证码错误");
        }

        if (UserConstants.NOT_UNIQUE.equals(sysUserService.checkPhoneUnique(user)))
        {
            return AjaxResult.error("注册失败，手机号码已存在");
        }
        user.setUserName(user.getPhonenumber());
        user.setNickName(user.getPhonenumber());
        user.setUserType("01");
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        return toAjax(sysUserService.insertUser(user));
    }


    @GetMapping("/viptime")
    public AjaxResult getUserVipTime(HttpServletRequest request) {
        LoginUser loginUser = tokenService.getLoginUser(request);
        SysUser sysUser = loginUser.getUser();
        AccUserAccount accUserAccount = iAccUserAccountService.selectAccUserAccountByUserId(sysUser.getUserId());
        AjaxResult ajax = AjaxResult.success(accUserAccount.getAvdate());
        return  ajax;
    }
}
