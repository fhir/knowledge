package com.huohuzhihui.web.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.huohuzhihui.api.service.ApiNoticeService;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.system.domain.SysNotice;

/**
 * 通知公告Controller
 * 
 * @author zylu
 * @date 2020-11-14
 */
@RestController
@RequestMapping("/api/notice")
public class ApiNoticeController extends BaseController
{
    @Autowired
    private ApiNoticeService apiNoticeService;

    @PostMapping("/findNoticeList")
    @ResponseBody
    public AjaxResult findNoticeList(SysNotice sysNotice){
        sysNotice.setStatus("0");
        List<SysNotice> list = apiNoticeService.findNoticeList(sysNotice);
        return AjaxResult.success(list);
    }
    @GetMapping("/getNotice")
    public AjaxResult getNotice(Long id){
        return new AjaxResult(200, "查询通知公告成功", apiNoticeService.getNoticeById(id));
    }



}
