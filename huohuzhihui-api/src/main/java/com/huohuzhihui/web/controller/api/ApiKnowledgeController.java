package com.huohuzhihui.web.controller.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.huohuzhihui.api.service.ApiKnowledgeService;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.knowledge.domain.KnowleageCourse;
import com.huohuzhihui.knowledge.domain.KnowledgeCategory;
import com.huohuzhihui.knowledge.domain.KnowledgeChapter;
import com.huohuzhihui.knowledge.service.IKnowledgeCategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商户Controller
 * 
 * @author zylu
 * @date 2020-11-14
 */
@Api("课程信息查询与管理")
@RestController
@RequestMapping("/api/knowledge")
public class ApiKnowledgeController extends BaseController
{
    @Autowired
    private ApiKnowledgeService apiKnowledgeService;
    
    @Autowired
    IKnowledgeCategoryService knowledgeCategoryService;


    @ApiOperation("获取课程分类列表")
    @PostMapping("/findKnowledgeCategoryList")
    @ResponseBody
    public AjaxResult findKnowledgeCategoryList(@RequestBody  KnowledgeCategory knowledgeCategory){
        List<KnowledgeCategory> list = apiKnowledgeService.findKnowledgeCategoryList(knowledgeCategory);
        return AjaxResult.success(list);
    }
    
    @ApiOperation("获取课程分类子分类列表")
    @PostMapping("/findKnowledgeCategoryChildren")
    @ResponseBody
    public AjaxResult findKnowledgeCategoryList(@RequestBody String data){
    	JSONObject json = JSON.parseObject(data);
    	String categoryCode = json.getString("goodstype");
    	Long flid = json.getLong("flid");
    	if(categoryCode!=null) {
	    	KnowledgeCategory root = knowledgeCategoryService.getKnowledgeCategoryByCode(categoryCode);
	    	if(root==null) {
	    		return AjaxResult.error(404,"分类编码不存在！");
	    	}
	    	flid = root.getCategoryId();
    	}
        List<KnowledgeCategory> list = knowledgeCategoryService.selectKnowledgeCategoryListChildren(flid);
        return AjaxResult.success(list);
    }
    

    @ApiOperation("获取和搜索课程列表")
    @PostMapping("/findKnowledgeCourseList")
    @ResponseBody
    public AjaxResult findKnowledgeCourseList(@RequestBody KnowleageCourse knowleageCourse){
        List<KnowleageCourse> list = apiKnowledgeService.findKnowledgeCourseList(knowleageCourse);
        return AjaxResult.success(list);
    }


    @PostMapping("/getCourseById")
    @ResponseBody
    public AjaxResult getCourseById(@RequestBody KnowleageCourse knowleageCourse){
        return AjaxResult.success(apiKnowledgeService.getCourseById(knowleageCourse));
    }
    
    
    @PostMapping("/findChapterListByCourseId")
    @ResponseBody
    public AjaxResult findChapterListByCourseId(@RequestBody KnowledgeChapter knowledgeChapter){
        List<KnowledgeChapter> list = apiKnowledgeService.findChapterListByCourseId(knowledgeChapter);
        return AjaxResult.success(list);
    }

    @PostMapping("/getChapterByChapterId")
    @ResponseBody
    public AjaxResult getChapterByChapterId(@RequestBody KnowledgeChapter knowledgeChapter){
        KnowledgeChapter result = apiKnowledgeService.getChapterByChapterId(knowledgeChapter.getChapterId());
        return AjaxResult.success(result);
    }

}
