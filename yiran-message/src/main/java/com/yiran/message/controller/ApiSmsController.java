package com.yiran.message.controller;


import com.alibaba.fastjson.JSON;

import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.core.domain.model.LoginUser;

import com.huohuzhihui.framework.web.service.TokenService;

import com.yiran.message.domain.SendAuthCodeRequest;
import com.yiran.message.service.impl.SmsTencentAuthCodeSendService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *微信api
 * 
 * @author lizhihe
 * @date 2020-11-22
 */
@RestController
@RequestMapping("/api/sms")
public class ApiSmsController extends BaseController
{
    @Autowired
    private SmsTencentAuthCodeSendService smsVerificationCodeService;


    @PostMapping("/sendCode")
    public AjaxResult sendCode(String mobile) {
    	SendAuthCodeRequest request = new SendAuthCodeRequest();
    	request.setPhone(mobile);
    	return smsVerificationCodeService.sendAuthCode(request);        
    }

}
