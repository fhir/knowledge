package com.yiran.message.domain;

import java.util.Date;

import com.huohuzhihui.common.core.domain.BaseEntity;

import lombok.Data;

@Data
public class Email extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/** 邮箱编号 */
	private Long emailId;
	
	/** 0：站内信；1：站外信 */
	private int emailSite;
	
	/** 用户编号，发送方 */
	private Long formUser;
	
	/** 用户编号，接收方 */
	private Long toUser;
	
	/** 接收方邮件地址 */
	private String toUserEmail;
	
	/** 邮件主题 */
	private String emailSubject;
	
	/** 邮件内容 */
	private String emailContent;
	
	/** 邮件状态(0：正常；1：已删除（注意进入垃圾箱内并非已删除，垃圾箱内的邮件可恢复）) */
	private int emailStatus;
	
	/** 邮件类型(例：工作，广告，文档等，可在字典中配置) */
	private String emailType;
	
	/** 邮件文件夹(例：收件箱，重要，草稿，垃圾箱等，可在字典中配置)) */
	private String emailFolder;
	
	/** 邮件标签(例：朋友；音乐，家庭，自定义标签，可在字典中配置) */
	private String emailLabel;
	
	/** 发送邮件状态(0：成功；1：失败) */
	private int sendStatus;	
	
	/** 发信人姓名*/
	private String userName;
	
	/** 传递 */
	private String toUserIds;
	
	private String toUserEmails;

}
