package com.huohuzhihui.distribution.mapper;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisUpgradeParam;

/**
 * 垂直升级配置Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface DisUpgradeParamMapper 
{
    /**
     * 查询垂直升级配置
     * 
     * @param id 垂直升级配置ID
     * @return 垂直升级配置
     */
    public DisUpgradeParam selectDisUpgradeParamById(Long id);

    /**
     * 查询垂直升级配置列表
     * 
     * @param disUpgradeParam 垂直升级配置
     * @return 垂直升级配置集合
     */
    public List<DisUpgradeParam> selectDisUpgradeParamList(DisUpgradeParam disUpgradeParam);

    /**
     * 新增垂直升级配置
     * 
     * @param disUpgradeParam 垂直升级配置
     * @return 结果
     */
    public int insertDisUpgradeParam(DisUpgradeParam disUpgradeParam);

    /**
     * 修改垂直升级配置
     * 
     * @param disUpgradeParam 垂直升级配置
     * @return 结果
     */
    public int updateDisUpgradeParam(DisUpgradeParam disUpgradeParam);

    /**
     * 删除垂直升级配置
     * 
     * @param id 垂直升级配置ID
     * @return 结果
     */
    public int deleteDisUpgradeParamById(Long id);

    /**
     * 批量删除垂直升级配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDisUpgradeParamByIds(Long[] ids);
}
