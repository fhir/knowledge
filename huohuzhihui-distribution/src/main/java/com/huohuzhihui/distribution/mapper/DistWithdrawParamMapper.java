package com.huohuzhihui.distribution.mapper;

import java.util.List;
import com.huohuzhihui.distribution.domain.DistWithdrawParam;

/**
 * 提现收费配置Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface DistWithdrawParamMapper 
{
    /**
     * 查询提现收费配置
     * 
     * @param id 提现收费配置ID
     * @return 提现收费配置
     */
    public DistWithdrawParam selectDistWithdrawParamById(Long id);

    /**
     * 查询提现收费配置列表
     * 
     * @param distWithdrawParam 提现收费配置
     * @return 提现收费配置集合
     */
    public List<DistWithdrawParam> selectDistWithdrawParamList(DistWithdrawParam distWithdrawParam);

    /**
     * 新增提现收费配置
     * 
     * @param distWithdrawParam 提现收费配置
     * @return 结果
     */
    public int insertDistWithdrawParam(DistWithdrawParam distWithdrawParam);

    /**
     * 修改提现收费配置
     * 
     * @param distWithdrawParam 提现收费配置
     * @return 结果
     */
    public int updateDistWithdrawParam(DistWithdrawParam distWithdrawParam);

    /**
     * 删除提现收费配置
     * 
     * @param id 提现收费配置ID
     * @return 结果
     */
    public int deleteDistWithdrawParamById(Long id);

    /**
     * 批量删除提现收费配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDistWithdrawParamByIds(Long[] ids);
}
