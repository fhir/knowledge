package com.huohuzhihui.distribution.mapper;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisRankIntegralRecord;

/**
 * 系统积分记录Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface DisRankIntegralRecordMapper 
{
    /**
     * 查询系统积分记录
     * 
     * @param id 系统积分记录ID
     * @return 系统积分记录
     */
    public DisRankIntegralRecord selectDisRankIntegralRecordById(Integer id);

    /**
     * 查询系统积分记录列表
     * 
     * @param disRankIntegralRecord 系统积分记录
     * @return 系统积分记录集合
     */
    public List<DisRankIntegralRecord> selectDisRankIntegralRecordList(DisRankIntegralRecord disRankIntegralRecord);

    /**
     * 新增系统积分记录
     * 
     * @param disRankIntegralRecord 系统积分记录
     * @return 结果
     */
    public int insertDisRankIntegralRecord(DisRankIntegralRecord disRankIntegralRecord);

    /**
     * 修改系统积分记录
     * 
     * @param disRankIntegralRecord 系统积分记录
     * @return 结果
     */
    public int updateDisRankIntegralRecord(DisRankIntegralRecord disRankIntegralRecord);

    /**
     * 删除系统积分记录
     * 
     * @param id 系统积分记录ID
     * @return 结果
     */
    public int deleteDisRankIntegralRecordById(Integer id);

    /**
     * 批量删除系统积分记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDisRankIntegralRecordByIds(Integer[] ids);
}
