package com.huohuzhihui.distribution.mapper;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisMemberAmount;

/**
 * 账户金额Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface DisMemberAmountMapper 
{
    /**
     * 查询账户金额
     * 
     * @param id 账户金额ID
     * @return 账户金额
     */
    public DisMemberAmount selectDisMemberAmountById(Long id);

    /**
     * 查询账户金额列表
     * 
     * @param disMemberAmount 账户金额
     * @return 账户金额集合
     */
    public List<DisMemberAmount> selectDisMemberAmountList(DisMemberAmount disMemberAmount);

    /**
     * 新增账户金额
     * 
     * @param disMemberAmount 账户金额
     * @return 结果
     */
    public int insertDisMemberAmount(DisMemberAmount disMemberAmount);

    /**
     * 修改账户金额
     * 
     * @param disMemberAmount 账户金额
     * @return 结果
     */
    public int updateDisMemberAmount(DisMemberAmount disMemberAmount);

    /**
     * 删除账户金额
     * 
     * @param id 账户金额ID
     * @return 结果
     */
    public int deleteDisMemberAmountById(Long id);

    /**
     * 批量删除账户金额
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDisMemberAmountByIds(Long[] ids);
}
