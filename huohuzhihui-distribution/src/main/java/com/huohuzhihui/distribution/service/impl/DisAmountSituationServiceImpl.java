package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisAmountSituationMapper;
import com.huohuzhihui.distribution.domain.DisAmountSituation;
import com.huohuzhihui.distribution.service.IDisAmountSituationService;

/**
 * 账户变动，用于记录账户变动情况Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisAmountSituationServiceImpl implements IDisAmountSituationService 
{
    @Autowired
    private DisAmountSituationMapper disAmountSituationMapper;

    /**
     * 查询账户变动，用于记录账户变动情况
     * 
     * @param id 账户变动，用于记录账户变动情况ID
     * @return 账户变动，用于记录账户变动情况
     */
    @Override
    public DisAmountSituation selectDisAmountSituationById(Long id)
    {
        return disAmountSituationMapper.selectDisAmountSituationById(id);
    }

    /**
     * 查询账户变动，用于记录账户变动情况列表
     * 
     * @param disAmountSituation 账户变动，用于记录账户变动情况
     * @return 账户变动，用于记录账户变动情况
     */
    @Override
    public List<DisAmountSituation> selectDisAmountSituationList(DisAmountSituation disAmountSituation)
    {
        return disAmountSituationMapper.selectDisAmountSituationList(disAmountSituation);
    }

    /**
     * 新增账户变动，用于记录账户变动情况
     * 
     * @param disAmountSituation 账户变动，用于记录账户变动情况
     * @return 结果
     */
    @Override
    public int insertDisAmountSituation(DisAmountSituation disAmountSituation)
    {
        return disAmountSituationMapper.insertDisAmountSituation(disAmountSituation);
    }

    /**
     * 修改账户变动，用于记录账户变动情况
     * 
     * @param disAmountSituation 账户变动，用于记录账户变动情况
     * @return 结果
     */
    @Override
    public int updateDisAmountSituation(DisAmountSituation disAmountSituation)
    {
        return disAmountSituationMapper.updateDisAmountSituation(disAmountSituation);
    }

    /**
     * 批量删除账户变动，用于记录账户变动情况
     * 
     * @param ids 需要删除的账户变动，用于记录账户变动情况ID
     * @return 结果
     */
    @Override
    public int deleteDisAmountSituationByIds(Long[] ids)
    {
        return disAmountSituationMapper.deleteDisAmountSituationByIds(ids);
    }

    /**
     * 删除账户变动，用于记录账户变动情况信息
     * 
     * @param id 账户变动，用于记录账户变动情况ID
     * @return 结果
     */
    @Override
    public int deleteDisAmountSituationById(Long id)
    {
        return disAmountSituationMapper.deleteDisAmountSituationById(id);
    }
}
