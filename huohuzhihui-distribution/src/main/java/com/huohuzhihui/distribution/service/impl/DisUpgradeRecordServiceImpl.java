package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisUpgradeRecordMapper;
import com.huohuzhihui.distribution.domain.DisUpgradeRecord;
import com.huohuzhihui.distribution.service.IDisUpgradeRecordService;

/**
 * 用户升级记录Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisUpgradeRecordServiceImpl implements IDisUpgradeRecordService 
{
    @Autowired
    private DisUpgradeRecordMapper disUpgradeRecordMapper;

    /**
     * 查询用户升级记录
     * 
     * @param id 用户升级记录ID
     * @return 用户升级记录
     */
    @Override
    public DisUpgradeRecord selectDisUpgradeRecordById(Integer id)
    {
        return disUpgradeRecordMapper.selectDisUpgradeRecordById(id);
    }

    /**
     * 查询用户升级记录列表
     * 
     * @param disUpgradeRecord 用户升级记录
     * @return 用户升级记录
     */
    @Override
    public List<DisUpgradeRecord> selectDisUpgradeRecordList(DisUpgradeRecord disUpgradeRecord)
    {
        return disUpgradeRecordMapper.selectDisUpgradeRecordList(disUpgradeRecord);
    }

    /**
     * 新增用户升级记录
     * 
     * @param disUpgradeRecord 用户升级记录
     * @return 结果
     */
    @Override
    public int insertDisUpgradeRecord(DisUpgradeRecord disUpgradeRecord)
    {
        return disUpgradeRecordMapper.insertDisUpgradeRecord(disUpgradeRecord);
    }

    /**
     * 修改用户升级记录
     * 
     * @param disUpgradeRecord 用户升级记录
     * @return 结果
     */
    @Override
    public int updateDisUpgradeRecord(DisUpgradeRecord disUpgradeRecord)
    {
        return disUpgradeRecordMapper.updateDisUpgradeRecord(disUpgradeRecord);
    }

    /**
     * 批量删除用户升级记录
     * 
     * @param ids 需要删除的用户升级记录ID
     * @return 结果
     */
    @Override
    public int deleteDisUpgradeRecordByIds(Integer[] ids)
    {
        return disUpgradeRecordMapper.deleteDisUpgradeRecordByIds(ids);
    }

    /**
     * 删除用户升级记录信息
     * 
     * @param id 用户升级记录ID
     * @return 结果
     */
    @Override
    public int deleteDisUpgradeRecordById(Integer id)
    {
        return disUpgradeRecordMapper.deleteDisUpgradeRecordById(id);
    }
}
