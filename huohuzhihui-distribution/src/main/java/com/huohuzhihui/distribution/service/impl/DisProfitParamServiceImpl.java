package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisProfitParamMapper;
import com.huohuzhihui.distribution.domain.DisProfitParam;
import com.huohuzhihui.distribution.service.IDisProfitParamService;

/**
 * 分润参数设置Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisProfitParamServiceImpl implements IDisProfitParamService 
{
    @Autowired
    private DisProfitParamMapper disProfitParamMapper;

    /**
     * 查询分润参数设置
     * 
     * @param id 分润参数设置ID
     * @return 分润参数设置
     */
    @Override
    public DisProfitParam selectDisProfitParamById(Long id)
    {
        return disProfitParamMapper.selectDisProfitParamById(id);
    }

    /**
     * 查询分润参数设置列表
     * 
     * @param disProfitParam 分润参数设置
     * @return 分润参数设置
     */
    @Override
    public List<DisProfitParam> selectDisProfitParamList(DisProfitParam disProfitParam)
    {
        return disProfitParamMapper.selectDisProfitParamList(disProfitParam);
    }

    /**
     * 新增分润参数设置
     * 
     * @param disProfitParam 分润参数设置
     * @return 结果
     */
    @Override
    public int insertDisProfitParam(DisProfitParam disProfitParam)
    {
        return disProfitParamMapper.insertDisProfitParam(disProfitParam);
    }

    /**
     * 修改分润参数设置
     * 
     * @param disProfitParam 分润参数设置
     * @return 结果
     */
    @Override
    public int updateDisProfitParam(DisProfitParam disProfitParam)
    {
        disProfitParam.setUpdateTime(DateUtils.getNowDate());
        return disProfitParamMapper.updateDisProfitParam(disProfitParam);
    }

    /**
     * 批量删除分润参数设置
     * 
     * @param ids 需要删除的分润参数设置ID
     * @return 结果
     */
    @Override
    public int deleteDisProfitParamByIds(Long[] ids)
    {
        return disProfitParamMapper.deleteDisProfitParamByIds(ids);
    }

    /**
     * 删除分润参数设置信息
     * 
     * @param id 分润参数设置ID
     * @return 结果
     */
    @Override
    public int deleteDisProfitParamById(Long id)
    {
        return disProfitParamMapper.deleteDisProfitParamById(id);
    }
}
