package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisWithdrawRecordMapper;
import com.huohuzhihui.distribution.domain.DisWithdrawRecord;
import com.huohuzhihui.distribution.service.IDisWithdrawRecordService;

/**
 * 提现Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisWithdrawRecordServiceImpl implements IDisWithdrawRecordService 
{
    @Autowired
    private DisWithdrawRecordMapper disWithdrawRecordMapper;

    /**
     * 查询提现
     * 
     * @param id 提现ID
     * @return 提现
     */
    @Override
    public DisWithdrawRecord selectDisWithdrawRecordById(Long id)
    {
        return disWithdrawRecordMapper.selectDisWithdrawRecordById(id);
    }

    /**
     * 查询提现列表
     * 
     * @param disWithdrawRecord 提现
     * @return 提现
     */
    @Override
    public List<DisWithdrawRecord> selectDisWithdrawRecordList(DisWithdrawRecord disWithdrawRecord)
    {
        return disWithdrawRecordMapper.selectDisWithdrawRecordList(disWithdrawRecord);
    }

    /**
     * 新增提现
     * 
     * @param disWithdrawRecord 提现
     * @return 结果
     */
    @Override
    public int insertDisWithdrawRecord(DisWithdrawRecord disWithdrawRecord)
    {
        return disWithdrawRecordMapper.insertDisWithdrawRecord(disWithdrawRecord);
    }

    /**
     * 修改提现
     * 
     * @param disWithdrawRecord 提现
     * @return 结果
     */
    @Override
    public int updateDisWithdrawRecord(DisWithdrawRecord disWithdrawRecord)
    {
        return disWithdrawRecordMapper.updateDisWithdrawRecord(disWithdrawRecord);
    }

    /**
     * 批量删除提现
     * 
     * @param ids 需要删除的提现ID
     * @return 结果
     */
    @Override
    public int deleteDisWithdrawRecordByIds(Long[] ids)
    {
        return disWithdrawRecordMapper.deleteDisWithdrawRecordByIds(ids);
    }

    /**
     * 删除提现信息
     * 
     * @param id 提现ID
     * @return 结果
     */
    @Override
    public int deleteDisWithdrawRecordById(Long id)
    {
        return disWithdrawRecordMapper.deleteDisWithdrawRecordById(id);
    }
}
