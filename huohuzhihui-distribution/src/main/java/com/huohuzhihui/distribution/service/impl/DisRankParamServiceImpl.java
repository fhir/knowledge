package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisRankParamMapper;
import com.huohuzhihui.distribution.domain.DisRankParam;
import com.huohuzhihui.distribution.service.IDisRankParamService;

/**
 * 分润参数设置Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisRankParamServiceImpl implements IDisRankParamService 
{
    @Autowired
    private DisRankParamMapper disRankParamMapper;

    /**
     * 查询分润参数设置
     * 
     * @param id 分润参数设置ID
     * @return 分润参数设置
     */
    @Override
    public DisRankParam selectDisRankParamById(Long id)
    {
        return disRankParamMapper.selectDisRankParamById(id);
    }

    /**
     * 查询分润参数设置列表
     * 
     * @param disRankParam 分润参数设置
     * @return 分润参数设置
     */
    @Override
    public List<DisRankParam> selectDisRankParamList(DisRankParam disRankParam)
    {
        return disRankParamMapper.selectDisRankParamList(disRankParam);
    }

    /**
     * 新增分润参数设置
     * 
     * @param disRankParam 分润参数设置
     * @return 结果
     */
    @Override
    public int insertDisRankParam(DisRankParam disRankParam)
    {
        return disRankParamMapper.insertDisRankParam(disRankParam);
    }

    /**
     * 修改分润参数设置
     * 
     * @param disRankParam 分润参数设置
     * @return 结果
     */
    @Override
    public int updateDisRankParam(DisRankParam disRankParam)
    {
        disRankParam.setUpdateTime(DateUtils.getNowDate());
        return disRankParamMapper.updateDisRankParam(disRankParam);
    }

    /**
     * 批量删除分润参数设置
     * 
     * @param ids 需要删除的分润参数设置ID
     * @return 结果
     */
    @Override
    public int deleteDisRankParamByIds(Long[] ids)
    {
        return disRankParamMapper.deleteDisRankParamByIds(ids);
    }

    /**
     * 删除分润参数设置信息
     * 
     * @param id 分润参数设置ID
     * @return 结果
     */
    @Override
    public int deleteDisRankParamById(Long id)
    {
        return disRankParamMapper.deleteDisRankParamById(id);
    }
}
