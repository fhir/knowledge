package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisRankIntegralRecordMapper;
import com.huohuzhihui.distribution.domain.DisRankIntegralRecord;
import com.huohuzhihui.distribution.service.IDisRankIntegralRecordService;

/**
 * 系统积分记录Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisRankIntegralRecordServiceImpl implements IDisRankIntegralRecordService 
{
    @Autowired
    private DisRankIntegralRecordMapper disRankIntegralRecordMapper;

    /**
     * 查询系统积分记录
     * 
     * @param id 系统积分记录ID
     * @return 系统积分记录
     */
    @Override
    public DisRankIntegralRecord selectDisRankIntegralRecordById(Integer id)
    {
        return disRankIntegralRecordMapper.selectDisRankIntegralRecordById(id);
    }

    /**
     * 查询系统积分记录列表
     * 
     * @param disRankIntegralRecord 系统积分记录
     * @return 系统积分记录
     */
    @Override
    public List<DisRankIntegralRecord> selectDisRankIntegralRecordList(DisRankIntegralRecord disRankIntegralRecord)
    {
        return disRankIntegralRecordMapper.selectDisRankIntegralRecordList(disRankIntegralRecord);
    }

    /**
     * 新增系统积分记录
     * 
     * @param disRankIntegralRecord 系统积分记录
     * @return 结果
     */
    @Override
    public int insertDisRankIntegralRecord(DisRankIntegralRecord disRankIntegralRecord)
    {
        return disRankIntegralRecordMapper.insertDisRankIntegralRecord(disRankIntegralRecord);
    }

    /**
     * 修改系统积分记录
     * 
     * @param disRankIntegralRecord 系统积分记录
     * @return 结果
     */
    @Override
    public int updateDisRankIntegralRecord(DisRankIntegralRecord disRankIntegralRecord)
    {
        return disRankIntegralRecordMapper.updateDisRankIntegralRecord(disRankIntegralRecord);
    }

    /**
     * 批量删除系统积分记录
     * 
     * @param ids 需要删除的系统积分记录ID
     * @return 结果
     */
    @Override
    public int deleteDisRankIntegralRecordByIds(Integer[] ids)
    {
        return disRankIntegralRecordMapper.deleteDisRankIntegralRecordByIds(ids);
    }

    /**
     * 删除系统积分记录信息
     * 
     * @param id 系统积分记录ID
     * @return 结果
     */
    @Override
    public int deleteDisRankIntegralRecordById(Integer id)
    {
        return disRankIntegralRecordMapper.deleteDisRankIntegralRecordById(id);
    }
}
