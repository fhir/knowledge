package com.huohuzhihui.distribution.service;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisProfitParam;

/**
 * 分润参数设置Service接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface IDisProfitParamService 
{
    /**
     * 查询分润参数设置
     * 
     * @param id 分润参数设置ID
     * @return 分润参数设置
     */
    public DisProfitParam selectDisProfitParamById(Long id);

    /**
     * 查询分润参数设置列表
     * 
     * @param disProfitParam 分润参数设置
     * @return 分润参数设置集合
     */
    public List<DisProfitParam> selectDisProfitParamList(DisProfitParam disProfitParam);

    /**
     * 新增分润参数设置
     * 
     * @param disProfitParam 分润参数设置
     * @return 结果
     */
    public int insertDisProfitParam(DisProfitParam disProfitParam);

    /**
     * 修改分润参数设置
     * 
     * @param disProfitParam 分润参数设置
     * @return 结果
     */
    public int updateDisProfitParam(DisProfitParam disProfitParam);

    /**
     * 批量删除分润参数设置
     * 
     * @param ids 需要删除的分润参数设置ID
     * @return 结果
     */
    public int deleteDisProfitParamByIds(Long[] ids);

    /**
     * 删除分润参数设置信息
     * 
     * @param id 分润参数设置ID
     * @return 结果
     */
    public int deleteDisProfitParamById(Long id);
}
