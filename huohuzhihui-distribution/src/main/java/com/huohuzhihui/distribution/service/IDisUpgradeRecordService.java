package com.huohuzhihui.distribution.service;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisUpgradeRecord;

/**
 * 用户升级记录Service接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface IDisUpgradeRecordService 
{
    /**
     * 查询用户升级记录
     * 
     * @param id 用户升级记录ID
     * @return 用户升级记录
     */
    public DisUpgradeRecord selectDisUpgradeRecordById(Integer id);

    /**
     * 查询用户升级记录列表
     * 
     * @param disUpgradeRecord 用户升级记录
     * @return 用户升级记录集合
     */
    public List<DisUpgradeRecord> selectDisUpgradeRecordList(DisUpgradeRecord disUpgradeRecord);

    /**
     * 新增用户升级记录
     * 
     * @param disUpgradeRecord 用户升级记录
     * @return 结果
     */
    public int insertDisUpgradeRecord(DisUpgradeRecord disUpgradeRecord);

    /**
     * 修改用户升级记录
     * 
     * @param disUpgradeRecord 用户升级记录
     * @return 结果
     */
    public int updateDisUpgradeRecord(DisUpgradeRecord disUpgradeRecord);

    /**
     * 批量删除用户升级记录
     * 
     * @param ids 需要删除的用户升级记录ID
     * @return 结果
     */
    public int deleteDisUpgradeRecordByIds(Integer[] ids);

    /**
     * 删除用户升级记录信息
     * 
     * @param id 用户升级记录ID
     * @return 结果
     */
    public int deleteDisUpgradeRecordById(Integer id);
}
