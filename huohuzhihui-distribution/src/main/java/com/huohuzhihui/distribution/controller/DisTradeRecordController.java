package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisTradeRecord;
import com.huohuzhihui.distribution.service.IDisTradeRecordService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 交易金额记录Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/trade")
public class DisTradeRecordController extends BaseController
{
    @Autowired
    private IDisTradeRecordService disTradeRecordService;

    /**
     * 查询交易金额记录列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisTradeRecord disTradeRecord)
    {
        startPage();
        List<DisTradeRecord> list = disTradeRecordService.selectDisTradeRecordList(disTradeRecord);
        return getDataTable(list);
    }

    /**
     * 导出交易金额记录列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:record:export')")
    @Log(title = "交易金额记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisTradeRecord disTradeRecord)
    {
        List<DisTradeRecord> list = disTradeRecordService.selectDisTradeRecordList(disTradeRecord);
        ExcelUtil<DisTradeRecord> util = new ExcelUtil<DisTradeRecord>(DisTradeRecord.class);
        return util.exportExcel(list, "record");
    }

    /**
     * 获取交易金额记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:record:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(disTradeRecordService.selectDisTradeRecordById(id));
    }

    /**
     * 新增交易金额记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:record:add')")
    @Log(title = "交易金额记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisTradeRecord disTradeRecord)
    {
        return toAjax(disTradeRecordService.insertDisTradeRecord(disTradeRecord));
    }

    /**
     * 修改交易金额记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:record:edit')")
    @Log(title = "交易金额记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisTradeRecord disTradeRecord)
    {
        return toAjax(disTradeRecordService.updateDisTradeRecord(disTradeRecord));
    }

    /**
     * 删除交易金额记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:record:remove')")
    @Log(title = "交易金额记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(disTradeRecordService.deleteDisTradeRecordByIds(ids));
    }
}
