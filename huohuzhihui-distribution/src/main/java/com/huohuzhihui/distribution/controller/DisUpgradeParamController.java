package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisUpgradeParam;
import com.huohuzhihui.distribution.service.IDisUpgradeParamService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 升级配置Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/upgradeParam")
public class DisUpgradeParamController extends BaseController
{
    @Autowired
    private IDisUpgradeParamService disUpgradeParamService;

    /**
     * 查询升级配置列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeParam:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisUpgradeParam disUpgradeParam)
    {
        startPage();
        List<DisUpgradeParam> list = disUpgradeParamService.selectDisUpgradeParamList(disUpgradeParam);
        return getDataTable(list);
    }

    /**
     * 导出升级配置列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeParam:export')")
    @Log(title = "升级配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisUpgradeParam disUpgradeParam)
    {
        List<DisUpgradeParam> list = disUpgradeParamService.selectDisUpgradeParamList(disUpgradeParam);
        ExcelUtil<DisUpgradeParam> util = new ExcelUtil<DisUpgradeParam>(DisUpgradeParam.class);
        return util.exportExcel(list, "upgradeParam");
    }

    /**
     * 获取升级配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeParam:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(disUpgradeParamService.selectDisUpgradeParamById(id));
    }

    /**
     * 新增升级配置
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeParam:add')")
    @Log(title = "升级配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisUpgradeParam disUpgradeParam)
    {
        return toAjax(disUpgradeParamService.insertDisUpgradeParam(disUpgradeParam));
    }

    /**
     * 修改升级配置
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeParam:edit')")
    @Log(title = "升级配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisUpgradeParam disUpgradeParam)
    {
        return toAjax(disUpgradeParamService.updateDisUpgradeParam(disUpgradeParam));
    }

    /**
     * 删除升级配置
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeParam:remove')")
    @Log(title = "升级配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(disUpgradeParamService.deleteDisUpgradeParamByIds(ids));
    }
}
