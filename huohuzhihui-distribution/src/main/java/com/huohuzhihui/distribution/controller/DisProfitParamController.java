package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisProfitParam;
import com.huohuzhihui.distribution.service.IDisProfitParamService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 分润参数设置Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/profit/param")
public class DisProfitParamController extends BaseController
{
    @Autowired
    private IDisProfitParamService disProfitParamService;

    /**
     * 查询分润参数设置列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:param:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisProfitParam disProfitParam)
    {
        startPage();
        List<DisProfitParam> list = disProfitParamService.selectDisProfitParamList(disProfitParam);
        return getDataTable(list);
    }

    /**
     * 导出分润参数设置列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:param:export')")
    @Log(title = "分润参数设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisProfitParam disProfitParam)
    {
        List<DisProfitParam> list = disProfitParamService.selectDisProfitParamList(disProfitParam);
        ExcelUtil<DisProfitParam> util = new ExcelUtil<DisProfitParam>(DisProfitParam.class);
        return util.exportExcel(list, "param");
    }

    /**
     * 获取分润参数设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:param:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(disProfitParamService.selectDisProfitParamById(id));
    }

    /**
     * 新增分润参数设置
     */
    @PreAuthorize("@ss.hasPermi('distribution:param:add')")
    @Log(title = "分润参数设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisProfitParam disProfitParam)
    {
        return toAjax(disProfitParamService.insertDisProfitParam(disProfitParam));
    }

    /**
     * 修改分润参数设置
     */
    @PreAuthorize("@ss.hasPermi('distribution:param:edit')")
    @Log(title = "分润参数设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisProfitParam disProfitParam)
    {
        return toAjax(disProfitParamService.updateDisProfitParam(disProfitParam));
    }

    /**
     * 删除分润参数设置
     */
    @PreAuthorize("@ss.hasPermi('distribution:param:remove')")
    @Log(title = "分润参数设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(disProfitParamService.deleteDisProfitParamByIds(ids));
    }
}
