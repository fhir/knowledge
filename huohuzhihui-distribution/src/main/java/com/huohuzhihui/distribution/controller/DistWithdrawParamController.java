package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DistWithdrawParam;
import com.huohuzhihui.distribution.service.IDistWithdrawParamService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 提现收费配置Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/withdrawParam")
public class DistWithdrawParamController extends BaseController
{
    @Autowired
    private IDistWithdrawParamService distWithdrawParamService;

    /**
     * 查询提现收费配置列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawParam:list')")
    @GetMapping("/list")
    public TableDataInfo list(DistWithdrawParam distWithdrawParam)
    {
        startPage();
        List<DistWithdrawParam> list = distWithdrawParamService.selectDistWithdrawParamList(distWithdrawParam);
        return getDataTable(list);
    }

    /**
     * 导出提现收费配置列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawParam:export')")
    @Log(title = "提现收费配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DistWithdrawParam distWithdrawParam)
    {
        List<DistWithdrawParam> list = distWithdrawParamService.selectDistWithdrawParamList(distWithdrawParam);
        ExcelUtil<DistWithdrawParam> util = new ExcelUtil<DistWithdrawParam>(DistWithdrawParam.class);
        return util.exportExcel(list, "withdrawParam");
    }

    /**
     * 获取提现收费配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawParam:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(distWithdrawParamService.selectDistWithdrawParamById(id));
    }

    /**
     * 新增提现收费配置
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawParam:add')")
    @Log(title = "提现收费配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DistWithdrawParam distWithdrawParam)
    {
        return toAjax(distWithdrawParamService.insertDistWithdrawParam(distWithdrawParam));
    }

    /**
     * 修改提现收费配置
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawParam:edit')")
    @Log(title = "提现收费配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DistWithdrawParam distWithdrawParam)
    {
        return toAjax(distWithdrawParamService.updateDistWithdrawParam(distWithdrawParam));
    }

    /**
     * 删除提现收费配置
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawParam:remove')")
    @Log(title = "提现收费配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(distWithdrawParamService.deleteDistWithdrawParamByIds(ids));
    }
}
