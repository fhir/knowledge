package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisWithdrawRecord;
import com.huohuzhihui.distribution.service.IDisWithdrawRecordService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 提现Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/withdrawRecord")
public class DisWithdrawRecordController extends BaseController
{
    @Autowired
    private IDisWithdrawRecordService disWithdrawRecordService;

    /**
     * 查询提现列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawRecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisWithdrawRecord disWithdrawRecord)
    {
        startPage();
        List<DisWithdrawRecord> list = disWithdrawRecordService.selectDisWithdrawRecordList(disWithdrawRecord);
        return getDataTable(list);
    }

    /**
     * 导出提现列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawRecord:export')")
    @Log(title = "提现", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisWithdrawRecord disWithdrawRecord)
    {
        List<DisWithdrawRecord> list = disWithdrawRecordService.selectDisWithdrawRecordList(disWithdrawRecord);
        ExcelUtil<DisWithdrawRecord> util = new ExcelUtil<DisWithdrawRecord>(DisWithdrawRecord.class);
        return util.exportExcel(list, "withdrawRecord");
    }

    /**
     * 获取提现详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(disWithdrawRecordService.selectDisWithdrawRecordById(id));
    }

    /**
     * 新增提现
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawRecord:add')")
    @Log(title = "提现", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisWithdrawRecord disWithdrawRecord)
    {
        return toAjax(disWithdrawRecordService.insertDisWithdrawRecord(disWithdrawRecord));
    }

    /**
     * 修改提现
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawRecord:edit')")
    @Log(title = "提现", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisWithdrawRecord disWithdrawRecord)
    {
        return toAjax(disWithdrawRecordService.updateDisWithdrawRecord(disWithdrawRecord));
    }

    /**
     * 删除提现
     */
    @PreAuthorize("@ss.hasPermi('distribution:withdrawRecord:remove')")
    @Log(title = "提现", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(disWithdrawRecordService.deleteDisWithdrawRecordByIds(ids));
    }
}
