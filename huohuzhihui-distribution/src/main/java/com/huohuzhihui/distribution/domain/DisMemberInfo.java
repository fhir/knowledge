package com.huohuzhihui.distribution.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 用户对象 dis_member_info
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DisMemberInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 平台 */
    @Excel(name = "平台")
    private String disPlatformId;

    /** 用户id */
    @Excel(name = "用户id")
    private String disUserId;

    /** 上级id */
    @Excel(name = "上级id")
    private String disParentId;

    /** 全路径 */
    @Excel(name = "全路径")
    private String disFullIndex;

    /** 用户名 */
    @Excel(name = "用户名")
    private String disUserName;

    /** 用户类型(会员:游客，经理、老板，代理商统一为1000，后面待启用) */
    @Excel(name = "用户类型(会员:游客，经理、老板，代理商统一为1000，后面待启用)")
    private String disUserType;

    /** 用户段位(青铜、黄金、白银等、初级代理商，中级代理商、高级代理商) */
    @Excel(name = "用户段位(青铜、黄金、白银等、初级代理商，中级代理商、高级代理商)")
    private String disUserRank;

    /** 备注 */
    @Excel(name = "备注")
    private String disNote;

    /** 添加时间 */
    @Excel(name = "添加时间")
    private String addTime;

    /** 删除状态 */
    @Excel(name = "删除状态")
    private String isDelete;

    /** 上级代理商id */
    @Excel(name = "上级代理商id")
    private String disPlatSuper;

    /** 代理商全路径 */
    @Excel(name = "代理商全路径")
    private String disPlatFullIndex;

    /** 代理商等级 */
    @Excel(name = "代理商等级")
    private Long disPlatLevel;

    /** 身份类型(0,会员，1：代理商) */
    @Excel(name = "身份类型(0,会员，1：代理商)")
    private String identityType;

    /** 段位积分(清零积分) */
    @Excel(name = "段位积分(清零积分)")
    private Integer rankIntegral;

    /** 段位总积分(不清零积分) */
    @Excel(name = "段位总积分(不清零积分)")
    private Integer totalRankIntegral;

    /** 限制状态，0为正常，1为禁止邀请用户 */
    @Excel(name = "限制状态，0为正常，1为禁止邀请用户")
    private Integer confineStatus;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDisPlatformId(String disPlatformId) 
    {
        this.disPlatformId = disPlatformId;
    }

    public String getDisPlatformId() 
    {
        return disPlatformId;
    }
    public void setDisUserId(String disUserId) 
    {
        this.disUserId = disUserId;
    }

    public String getDisUserId() 
    {
        return disUserId;
    }
    public void setDisParentId(String disParentId) 
    {
        this.disParentId = disParentId;
    }

    public String getDisParentId() 
    {
        return disParentId;
    }
    public void setDisFullIndex(String disFullIndex) 
    {
        this.disFullIndex = disFullIndex;
    }

    public String getDisFullIndex() 
    {
        return disFullIndex;
    }
    public void setDisUserName(String disUserName) 
    {
        this.disUserName = disUserName;
    }

    public String getDisUserName() 
    {
        return disUserName;
    }
    public void setDisUserType(String disUserType) 
    {
        this.disUserType = disUserType;
    }

    public String getDisUserType() 
    {
        return disUserType;
    }
    public void setDisUserRank(String disUserRank) 
    {
        this.disUserRank = disUserRank;
    }

    public String getDisUserRank() 
    {
        return disUserRank;
    }
    public void setDisNote(String disNote) 
    {
        this.disNote = disNote;
    }

    public String getDisNote() 
    {
        return disNote;
    }
    public void setAddTime(String addTime) 
    {
        this.addTime = addTime;
    }

    public String getAddTime() 
    {
        return addTime;
    }
    public void setIsDelete(String isDelete) 
    {
        this.isDelete = isDelete;
    }

    public String getIsDelete() 
    {
        return isDelete;
    }
    public void setDisPlatSuper(String disPlatSuper) 
    {
        this.disPlatSuper = disPlatSuper;
    }

    public String getDisPlatSuper() 
    {
        return disPlatSuper;
    }
    public void setDisPlatFullIndex(String disPlatFullIndex) 
    {
        this.disPlatFullIndex = disPlatFullIndex;
    }

    public String getDisPlatFullIndex() 
    {
        return disPlatFullIndex;
    }
    public void setDisPlatLevel(Long disPlatLevel) 
    {
        this.disPlatLevel = disPlatLevel;
    }

    public Long getDisPlatLevel() 
    {
        return disPlatLevel;
    }
    public void setIdentityType(String identityType) 
    {
        this.identityType = identityType;
    }

    public String getIdentityType() 
    {
        return identityType;
    }
    public void setRankIntegral(Integer rankIntegral) 
    {
        this.rankIntegral = rankIntegral;
    }

    public Integer getRankIntegral() 
    {
        return rankIntegral;
    }
    public void setTotalRankIntegral(Integer totalRankIntegral) 
    {
        this.totalRankIntegral = totalRankIntegral;
    }

    public Integer getTotalRankIntegral() 
    {
        return totalRankIntegral;
    }
    public void setConfineStatus(Integer confineStatus) 
    {
        this.confineStatus = confineStatus;
    }

    public Integer getConfineStatus() 
    {
        return confineStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("disPlatformId", getDisPlatformId())
            .append("disUserId", getDisUserId())
            .append("disParentId", getDisParentId())
            .append("disFullIndex", getDisFullIndex())
            .append("disUserName", getDisUserName())
            .append("disUserType", getDisUserType())
            .append("disUserRank", getDisUserRank())
            .append("disNote", getDisNote())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .append("disPlatSuper", getDisPlatSuper())
            .append("disPlatFullIndex", getDisPlatFullIndex())
            .append("disPlatLevel", getDisPlatLevel())
            .append("identityType", getIdentityType())
            .append("rankIntegral", getRankIntegral())
            .append("totalRankIntegral", getTotalRankIntegral())
            .append("confineStatus", getConfineStatus())
            .toString();
    }
}
