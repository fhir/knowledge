package com.huohuzhihui.distribution.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 用户升级记录对象 dis_upgrade_record
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DisUpgradeRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Integer id;

    /** 用户id */
    @Excel(name = "用户id")
    private String disUserId;

    /** 升级前等级 */
    @Excel(name = "升级前等级")
    private String beforeUpgradeLevel;

    /** 升级后等级 */
    @Excel(name = "升级后等级")
    private String afterUpgradeLevel;

    /** 等级差额 */
    @Excel(name = "等级差额")
    private String levelDiffer;

    /** 升级时间 */
    @Excel(name = "升级时间")
    private String upgradeTime;

    /** 升级类型(0:垂直升级 1：水平升级) */
    @Excel(name = "升级类型(0:垂直升级 1：水平升级)")
    private String levelType;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setDisUserId(String disUserId) 
    {
        this.disUserId = disUserId;
    }

    public String getDisUserId() 
    {
        return disUserId;
    }
    public void setBeforeUpgradeLevel(String beforeUpgradeLevel) 
    {
        this.beforeUpgradeLevel = beforeUpgradeLevel;
    }

    public String getBeforeUpgradeLevel() 
    {
        return beforeUpgradeLevel;
    }
    public void setAfterUpgradeLevel(String afterUpgradeLevel) 
    {
        this.afterUpgradeLevel = afterUpgradeLevel;
    }

    public String getAfterUpgradeLevel() 
    {
        return afterUpgradeLevel;
    }
    public void setLevelDiffer(String levelDiffer) 
    {
        this.levelDiffer = levelDiffer;
    }

    public String getLevelDiffer() 
    {
        return levelDiffer;
    }
    public void setUpgradeTime(String upgradeTime) 
    {
        this.upgradeTime = upgradeTime;
    }

    public String getUpgradeTime() 
    {
        return upgradeTime;
    }
    public void setLevelType(String levelType) 
    {
        this.levelType = levelType;
    }

    public String getLevelType() 
    {
        return levelType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("disUserId", getDisUserId())
            .append("beforeUpgradeLevel", getBeforeUpgradeLevel())
            .append("afterUpgradeLevel", getAfterUpgradeLevel())
            .append("levelDiffer", getLevelDiffer())
            .append("upgradeTime", getUpgradeTime())
            .append("levelType", getLevelType())
            .toString();
    }
}
