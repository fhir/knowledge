package com.huohuzhihui.wxmp.mapper;

import java.util.List;
import com.huohuzhihui.wxmp.domain.WxAccount;

/**
 * 公众号账号Mapper接口
 * 
 * @author huohuzhihui
 * @date 2021-07-25
 */
public interface WxAccountMapper 
{
    /**
     * 查询公众号账号
     * 
     * @param appid 公众号账号ID
     * @return 公众号账号
     */
    public WxAccount selectWxAccountById(String appid);

    /**
     * 查询公众号账号列表
     * 
     * @param wxAccount 公众号账号
     * @return 公众号账号集合
     */
    public List<WxAccount> selectWxAccountList(WxAccount wxAccount);

    /**
     * 新增公众号账号
     * 
     * @param wxAccount 公众号账号
     * @return 结果
     */
    public int insertWxAccount(WxAccount wxAccount);

    /**
     * 修改公众号账号
     * 
     * @param wxAccount 公众号账号
     * @return 结果
     */
    public int updateWxAccount(WxAccount wxAccount);

    /**
     * 删除公众号账号
     * 
     * @param appid 公众号账号ID
     * @return 结果
     */
    public int deleteWxAccountById(String appid);

    /**
     * 批量删除公众号账号
     * 
     * @param appids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWxAccountByIds(String[] appids);
}
