package com.huohuzhihui.wxmp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 公众号账号对象 wx_account
 * 
 * @author huohuzhihui
 * @date 2021-07-25
 */
public class WxAccount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** appid */
    @Excel(name = "appid")
    private String appid;

    /** 主键id */
    private Long id;

    /** 公众号名称 */
    @Excel(name = "公众号名称")
    private String name;

    /** 账号类型 */
    @Excel(name = "账号类型")
    private Integer type;

    /** 认证状态 */
    @Excel(name = "认证状态")
    private Integer verified;

    /** appsecret */
    @Excel(name = "appsecret")
    private String secret;

    /** token */
    @Excel(name = "token")
    private String token;

    /** aesKey */
    @Excel(name = "aesKey")
    private String aesKey;

    public void setAppid(String appid) 
    {
        this.appid = appid;
    }

    public String getAppid() 
    {
        return appid;
    }
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setVerified(Integer verified) 
    {
        this.verified = verified;
    }

    public Integer getVerified() 
    {
        return verified;
    }
    public void setSecret(String secret) 
    {
        this.secret = secret;
    }

    public String getSecret() 
    {
        return secret;
    }
    public void setToken(String token) 
    {
        this.token = token;
    }

    public String getToken() 
    {
        return token;
    }
    public void setAesKey(String aesKey) 
    {
        this.aesKey = aesKey;
    }

    public String getAesKey() 
    {
        return aesKey;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("appid", getAppid())
            .append("id", getId())
            .append("name", getName())
            .append("type", getType())
            .append("verified", getVerified())
            .append("secret", getSecret())
            .append("token", getToken())
            .append("aesKey", getAesKey())
            .toString();
    }
}
