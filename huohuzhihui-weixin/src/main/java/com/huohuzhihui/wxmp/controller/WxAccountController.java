package com.huohuzhihui.wxmp.controller;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpCookie;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.wxmp.domain.WxAccount;
import com.huohuzhihui.wxmp.service.IWxAccountService;

import cn.hutool.http.server.HttpServerRequest;

import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 公众号账号Controller
 * 
 * @author huohuzhihui
 * @date 2021-07-25
 */
@RestController
@RequestMapping("/wxmp/account")
public class WxAccountController extends BaseController
{
    @Autowired
    private IWxAccountService wxAccountService;

    /**
     * 查询公众号账号列表
     */
    @PreAuthorize("@ss.hasPermi('wxmp:account:list')")
    @GetMapping("/list")
    public TableDataInfo list(WxAccount wxAccount)
    {
        startPage();
        List<WxAccount> list = wxAccountService.selectWxAccountList(wxAccount);
        return getDataTable(list);
    }

    /**
     * 导出公众号账号列表
     */
    @PreAuthorize("@ss.hasPermi('wxmp:account:export')")
    @Log(title = "公众号账号", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WxAccount wxAccount)
    {
        List<WxAccount> list = wxAccountService.selectWxAccountList(wxAccount);
        ExcelUtil<WxAccount> util = new ExcelUtil<WxAccount>(WxAccount.class);
        return util.exportExcel(list, "account");
    }

    /**
     * 获取公众号账号详细信息
     */
    @PreAuthorize("@ss.hasPermi('wxmp:account:query')")
    @GetMapping(value = "/{appid}")
    public AjaxResult getInfo(@PathVariable("appid") String appid)
    {
        return AjaxResult.success(wxAccountService.selectWxAccountById(appid));
    }

    /**
     * 新增公众号账号
     */
    @PreAuthorize("@ss.hasPermi('wxmp:account:add')")
    @Log(title = "公众号账号", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WxAccount wxAccount)
    {
        return toAjax(wxAccountService.insertWxAccount(wxAccount));
    }

    /**
     * 修改公众号账号
     */
    @PreAuthorize("@ss.hasPermi('wxmp:account:edit')")
    @Log(title = "公众号账号", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WxAccount wxAccount)
    {
        return toAjax(wxAccountService.updateWxAccount(wxAccount));
    }

    /**
     * 删除公众号账号
     */
    @PreAuthorize("@ss.hasPermi('wxmp:account:remove')")
    @Log(title = "公众号账号", businessType = BusinessType.DELETE)
	@DeleteMapping("/{appids}")
    public AjaxResult remove(@PathVariable String[] appids)
    {
        return toAjax(wxAccountService.deleteWxAccountByIds(appids));
    }
    
    /**
     *  将当前公众号账号设为默认
     */
    @PreAuthorize("@ss.hasPermi('wxmp:account:edit')")
    @Log(title = "公众号账号", businessType = BusinessType.INSERT)
    @GetMapping("setDefault/{appid}")
    public AjaxResult setDefault(@PathVariable String appid,HttpServletResponse response)
    {
    	Cookie cookie = new Cookie("mpAppId", appid);
    	cookie.setMaxAge(3600*24*30);
    	response.addCookie(cookie);
        return this.toAjax(1);
    }
}
