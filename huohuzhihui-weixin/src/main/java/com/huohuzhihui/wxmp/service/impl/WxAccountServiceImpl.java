package com.huohuzhihui.wxmp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.wxmp.mapper.WxAccountMapper;
import com.huohuzhihui.wxmp.domain.WxAccount;
import com.huohuzhihui.wxmp.service.IWxAccountService;

/**
 * 公众号账号Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2021-07-25
 */
@Service
public class WxAccountServiceImpl implements IWxAccountService 
{
    @Autowired
    private WxAccountMapper wxAccountMapper;

    /**
     * 查询公众号账号
     * 
     * @param appid 公众号账号ID
     * @return 公众号账号
     */
    @Override
    public WxAccount selectWxAccountById(String appid)
    {
        return wxAccountMapper.selectWxAccountById(appid);
    }

    /**
     * 查询公众号账号列表
     * 
     * @param wxAccount 公众号账号
     * @return 公众号账号
     */
    @Override
    public List<WxAccount> selectWxAccountList(WxAccount wxAccount)
    {
        return wxAccountMapper.selectWxAccountList(wxAccount);
    }

    /**
     * 新增公众号账号
     * 
     * @param wxAccount 公众号账号
     * @return 结果
     */
    @Override
    public int insertWxAccount(WxAccount wxAccount)
    {
        return wxAccountMapper.insertWxAccount(wxAccount);
    }

    /**
     * 修改公众号账号
     * 
     * @param wxAccount 公众号账号
     * @return 结果
     */
    @Override
    public int updateWxAccount(WxAccount wxAccount)
    {
        return wxAccountMapper.updateWxAccount(wxAccount);
    }

    /**
     * 批量删除公众号账号
     * 
     * @param appids 需要删除的公众号账号ID
     * @return 结果
     */
    @Override
    public int deleteWxAccountByIds(String[] appids)
    {
        return wxAccountMapper.deleteWxAccountByIds(appids);
    }

    /**
     * 删除公众号账号信息
     * 
     * @param appid 公众号账号ID
     * @return 结果
     */
    @Override
    public int deleteWxAccountById(String appid)
    {
        return wxAccountMapper.deleteWxAccountById(appid);
    }
}
