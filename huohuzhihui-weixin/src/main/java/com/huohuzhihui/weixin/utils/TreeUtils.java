package com.huohuzhihui.weixin.utils;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.huohuzhihui.common.core.domain.entity.SysMenu;
import com.huohuzhihui.mp.entity.WxMenu;



/**
 * 权限数据处理
 * 
 * @author yiran
 */
public class TreeUtils
{

    /**
     * 根据父节点的ID获取所有子节点
     * 
     * @param list 分类表
     * @param typeId 传入的父节点ID
     * @return String
     */
    public static List<SysMenu> getChildPerms(List<SysMenu> list, int parentId)
    {
        List<SysMenu> returnList = new ArrayList<SysMenu>();
        for (Iterator<SysMenu> iterator = list.iterator(); iterator.hasNext();)
        {
        	SysMenu t = (SysMenu) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId() == parentId)
            {
                recursionFn(list, t);
                returnList.add(t);
            }
        }
        return returnList;
    }
    
    public static List<WxMenu> getWeixinMenuChildPerms(List<WxMenu> list, String parentId)
    {
    	Map<String,List<WxMenu>> tree = new HashMap<>();
        List<WxMenu> returnList = new ArrayList<WxMenu>();
        for (Iterator<WxMenu> iterator = list.iterator(); iterator.hasNext();)
        {
        	WxMenu t = (WxMenu) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId().equals(parentId))
            {
            	recursionFnWeixinMenu(list, t, tree);
                returnList.add(t);
            }
        }
        return returnList;
    }

    /**
     * 递归列表
     * 
     * @param list
     * @param Menu
     */
    private static void recursionFnWeixinMenu(List<WxMenu> list, WxMenu t, Map<String,List<WxMenu>> tree)
    {
        // 得到子节点列表
        List<WxMenu> childList = getChildListWeixinMenu(list, t);
        tree.put(t.getId(),childList);
        for (WxMenu tChild : childList)
        {
            if (hasChildWeixinMenu(list, tChild))
            {
                // 判断是否有子节点
                Iterator<WxMenu> it = childList.iterator();
                while (it.hasNext())
                {
                	WxMenu n = (WxMenu) it.next();
                	recursionFnWeixinMenu(list, n, tree);
                }
            }
        }
    }

    private static void recursionFn(List<SysMenu> list, SysMenu t)
    {
        // 得到子节点列表
        List<SysMenu> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysMenu tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                // 判断是否有子节点
                Iterator<SysMenu> it = childList.iterator();
                while (it.hasNext())
                {
                	SysMenu n = (SysMenu) it.next();
                    recursionFn(list, n);
                }
            }
        }
    }

    
    /**
     * 得到子节点列表
     */
    private static List<WxMenu> getChildListWeixinMenu(List<WxMenu> list, WxMenu t)
    {

        List<WxMenu> tlist = new ArrayList<WxMenu>();
        Iterator<WxMenu> it = list.iterator();
        while (it.hasNext())
        {
        	WxMenu n = (WxMenu) it.next();
            if (n.getParentId().equals(t.getId()))
            {
                tlist.add(n);
            }
        }
        return tlist;
    }
    
    private static List<SysMenu> getChildList(List<SysMenu> list, SysMenu t)
    {

        List<SysMenu> tlist = new ArrayList<SysMenu>();
        Iterator<SysMenu> it = list.iterator();
        while (it.hasNext())
        {
        	SysMenu n = (SysMenu) it.next();
            if (n.getParentId().longValue() == t.getMenuId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    List<SysMenu> returnList = new ArrayList<SysMenu>();

    /**
     * 根据父节点的ID获取所有子节点
     * 
     * @param list 分类表
     * @param typeId 传入的父节点ID
     * @param prefix 子节点前缀
     */
    public List<SysMenu> getChildPerms(List<SysMenu> list, int typeId, String prefix)
    {
        if (list == null)
        {
            return null;
        }
        for (Iterator<SysMenu> iterator = list.iterator(); iterator.hasNext();)
        {
        	SysMenu node = (SysMenu) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (node.getParentId() .equals(typeId))
            {
                recursionFn(list, node, prefix);
            }
            // 二、遍历所有的父节点下的所有子节点
            /*
             * if (node.getParentId()==0) { recursionFn(list, node); }
             */
        }
        return returnList;
    }

    private void recursionFn(List<SysMenu> list, SysMenu node, String p)
    {
        // 得到子节点列表
        List<SysMenu> childList = getChildList(list, node);
        if (hasChild(list, node))
        {
            // 判断是否有子节点
            returnList.add(node);
            Iterator<SysMenu> it = childList.iterator();
            while (it.hasNext())
            {
            	SysMenu n = (SysMenu) it.next();
                n.setMenuName(p + n.getMenuName());
                recursionFn(list, n, p + p);
            }
        }
        else
        {
            returnList.add(node);
        }
    }

    /**
     * 判断是否有子节点
     */
    private static boolean hasChildWeixinMenu(List<WxMenu> list, WxMenu t)
    {
        return getChildListWeixinMenu(list, t).size() > 0 ? true : false;
    }
    
    private static boolean hasChild(List<SysMenu> list, SysMenu t)
    {
        return getChildList(list, t).size() > 0 ? true : false;
    }
}
