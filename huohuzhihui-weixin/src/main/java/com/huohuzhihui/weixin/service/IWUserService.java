package com.huohuzhihui.weixin.service;


import com.huohuzhihui.weixin.entity.WUser;

public interface IWUserService {
	public WUser queryByOpenid(String openid);
	public String queryOpenidByCode(String code);
}
