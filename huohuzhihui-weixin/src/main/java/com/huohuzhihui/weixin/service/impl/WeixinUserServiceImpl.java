package com.huohuzhihui.weixin.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.huohuzhihui.common.core.text.Convert;
import com.huohuzhihui.common.utils.StringUtils;
import com.huohuzhihui.mp.entity.WxUser;
import com.huohuzhihui.mp.mapper.WxUserMapper;


import com.huohuzhihui.weixin.service.IWeixinUserService;

/**
 * 公众号微信用户 服务层实现
 * 
 * @author yiran
 * @date 2018-08-19
 */
@Service
public class WeixinUserServiceImpl implements IWeixinUserService
{
	@Autowired
	private WxUserMapper weixinUserMapper;

	/**
     * 查询公众号微信用户信息
     * 
     * @param id 公众号微信用户ID
     * @return 公众号微信用户信息
     */
    @Override
	public WxUser selectWeixinUserById(String id)
	{
	    return weixinUserMapper.selectById(id);
	}
	
	/**
     * 查询公众号微信用户列表
     * 
     * @param weixinUser 公众号微信用户信息
     * @return 公众号微信用户集合
     */
	@Override
	public List<WxUser> selectWeixinUserList(WxUser weixinUser)
	{
		Map<String,Object> map = new HashMap<>();
	    return weixinUserMapper.selectWeixinUserList(weixinUser);
	}
	
    /**
     * 新增公众号微信用户
     * 
     * @param weixinUser 公众号微信用户信息
     * @return 结果
     */
	@Override
	public int insertWeixinUser(WxUser weixinUser)
	{
	    return weixinUserMapper.insert(weixinUser);
	}
	
	/**
     * 修改公众号微信用户
     * 
     * @param weixinUser 公众号微信用户信息
     * @return 结果
     */
	@Override
	public int updateWeixinUser(WxUser weixinUser)
	{
	    return weixinUserMapper.updateById(weixinUser);
	}
	
	/**
     * 保存公众号微信用户
     * 
     * @param weixinUser 公众号微信用户信息
     * @return 结果
     */
	@Override
	public int saveWeixinUser(WxUser weixinUser)
	{
	    String id = weixinUser.getId();
		int rows = 0;
		if (StringUtils.isNotNull(id))
        {
		    rows = weixinUserMapper.updateById(weixinUser);
		}
		else
        {
		    rows = weixinUserMapper.insert(weixinUser);
		}
		return rows;
	}
	
	/**
     * 删除公众号微信用户对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteWeixinUserByIds(String ids)
	{
		return weixinUserMapper.deleteBatchIds(List.of(Convert.toStrArray(ids)));
	}

	@Override
	public WxUser selectWeixinUserByOpenId(String openid) {
		return weixinUserMapper.selectById(openid);
	}
	
}
