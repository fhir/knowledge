package com.huohuzhihui.weixin.service;


import java.util.List;

import com.huohuzhihui.mp.entity.WxUser;


/**
 * 公众号微信用户 服务层
 * 
 * @author yiran
 * @date 2018-08-19
 */
public interface IWeixinUserService 
{
	/**
     * 查询公众号微信用户信息
     * 
     * @param id 公众号微信用户ID
     * @return 公众号微信用户信息
     */
	public WxUser selectWeixinUserById(String id);
	/**
	 * 查询公众号微信用户信息
	 * @param openid 公众号微信用户OPENID
	 * @return 公众号微信用户信息
	 */
	public WxUser selectWeixinUserByOpenId(String openid);
	
	/**
     * 查询公众号微信用户列表
     * 
     * @param weixinUser 公众号微信用户信息
     * @return 公众号微信用户集合
     */
	public List<WxUser> selectWeixinUserList(WxUser weixinUser);
	
	/**
     * 新增公众号微信用户
     * 
     * @param weixinUser 公众号微信用户信息
     * @return 结果
     */
	public int insertWeixinUser(WxUser weixinUser);
	
	/**
     * 修改公众号微信用户
     * 
     * @param weixinUser 公众号微信用户信息
     * @return 结果
     */
	public int updateWeixinUser(WxUser weixinUser);
	
	/**
     * 保存公众号微信用户
     * 
     * @param weixinUser 公众号微信用户信息
     * @return 结果
     */
	public int saveWeixinUser(WxUser weixinUser);
	
	
	/**
     * 删除公众号微信用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteWeixinUserByIds(String ids);
	
}
