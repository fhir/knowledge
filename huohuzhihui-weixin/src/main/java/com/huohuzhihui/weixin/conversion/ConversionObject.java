package com.huohuzhihui.weixin.conversion;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

import com.huohuzhihui.mp.entity.WxUser;
import com.huohuzhihui.weixin.entity.WUser;
import cn.hutool.core.util.RandomUtil;

/**
 * 对象转换类
 * @author pandaa
 *
 */
public class ConversionObject {
	static ZoneOffset zoneOffet = ZoneOffset.UTC;
	
	public static WxUser conversionToWeixinUser (WUser wUser){
		WxUser weixinUser = new WxUser();
		//UUID生成id
		weixinUser.setId(wUser.getOpenid());
		weixinUser.setSubscribe(""+wUser.getSubscribe());
		weixinUser.setOpenId(wUser.getOpenid());
		weixinUser.setNickName(wUser.getNickname());
		weixinUser.setSex(wUser.getSex());
		weixinUser.setCity(wUser.getCity());
		weixinUser.setCountry(wUser.getCountry());
		weixinUser.setProvince(wUser.getProvince());
		weixinUser.setLanguage(wUser.getLanguage());
		weixinUser.setHeadimgUrl(wUser.getHeadimgurl());
		weixinUser.setSubscribeTime(LocalDateTime.ofEpochSecond(wUser.getSubscribe_time(),0,zoneOffet));
		weixinUser.setUnionId(wUser.getUnionid());
		weixinUser.setRemark(wUser.getRemark());
		weixinUser.setGroupId(String.valueOf(wUser.getGroupid()));
		return weixinUser;
	}	
}
