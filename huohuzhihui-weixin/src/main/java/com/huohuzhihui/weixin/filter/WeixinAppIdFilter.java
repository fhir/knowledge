package com.huohuzhihui.weixin.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import com.huohuzhihui.common.utils.StringUtils;

import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import me.chanjar.weixin.mp.util.WxMpConfigStorageHolder;

/**
 * Repeatable 过滤器
 * 
 * @author ruoyi
 */
public class WeixinAppIdFilter implements Filter
{
	
	public WxMpConfigStorage wxMpConfigStorage;
	
    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        
        String appid = request.getParameter("appId");
        if (!StringUtils.isEmpty(appid))
        {
        	WxMpConfigStorageHolder.set(appid);
        }
        else if(request instanceof HttpServletRequest){        	
        	Cookie[] cookies = ((HttpServletRequest)request).getCookies();
            if (null != cookies) {
                for (Cookie cookie : cookies) {
                    if(cookie.getName().equals("mpAppId")) {
                    	appid = cookie.getValue();
                    	WxMpConfigStorageHolder.set(appid);
                    	break;
                    }
                }
            }
        }
        if(StringUtils.isEmpty(appid) && wxMpConfigStorage!=null){
        	WxMpConfigStorageHolder.set(wxMpConfigStorage.getAppId());
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy()
    {
    	
    }
}
