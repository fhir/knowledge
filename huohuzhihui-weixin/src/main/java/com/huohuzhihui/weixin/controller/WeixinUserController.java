package com.huohuzhihui.weixin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.mp.entity.WxUser;
import com.huohuzhihui.weixin.service.IWeixinUserService;

/**
 * 微信关注用户Controller
 * 
 * @author huohuzhihui
 * @date 2021-01-01
 */
@RestController
@RequestMapping("/weixin/weixinUser")
public class WeixinUserController extends BaseController
{
    @Autowired
    private IWeixinUserService weixinUserService;

    /**
     * 查询微信关注用户列表
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinUser:list')")
    @GetMapping("/list")
    public TableDataInfo list(WxUser weixinUser)
    {
        startPage();
        List<WxUser> list = weixinUserService.selectWeixinUserList(weixinUser);
        return getDataTable(list);
    }

    /**
     * 导出微信关注用户列表
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinUser:export')")
    @Log(title = "微信关注用户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WxUser weixinUser)
    {
        List<WxUser> list = weixinUserService.selectWeixinUserList(weixinUser);
        ExcelUtil<WxUser> util = new ExcelUtil<WxUser>(WxUser.class);
        return util.exportExcel(list, "weixinUser");
    }

    /**
     * 获取微信关注用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinUser:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(weixinUserService.selectWeixinUserById(id));
    }

    /**
     * 新增微信关注用户
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinUser:add')")
    @Log(title = "微信关注用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WxUser weixinUser)
    {
        return toAjax(weixinUserService.insertWeixinUser(weixinUser));
    }

    /**
     * 修改微信关注用户
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinUser:edit')")
    @Log(title = "微信关注用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WxUser weixinUser)
    {
        return toAjax(weixinUserService.updateWeixinUser(weixinUser));
    }

    /**
     * 删除微信关注用户
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinUser:remove')")
    @Log(title = "微信关注用户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(weixinUserService.deleteWeixinUserByIds(ids.toString()));
    }
}
