package com.huohuzhihui.weixin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.weixin.domain.WeixinTemplate;
import com.huohuzhihui.weixin.service.IWeixinTemplateService;

/**
 * 微信模板Controller
 * 
 * @author huohuzhihui
 * @date 2021-01-01
 */
@RestController
@RequestMapping("/weixin/weixinTemplate")
public class WeixinTemplateController extends BaseController
{
    @Autowired
    private IWeixinTemplateService weixinTemplateService;

    /**
     * 查询微信模板列表
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinTemplate:list')")
    @GetMapping("/list")
    public TableDataInfo list(WeixinTemplate weixinTemplate)
    {
        startPage();
        List<WeixinTemplate> list = weixinTemplateService.selectWeixinTemplateList(weixinTemplate);
        return getDataTable(list);
    }

    /**
     * 导出微信模板列表
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinTemplate:export')")
    @Log(title = "微信模板", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WeixinTemplate weixinTemplate)
    {
        List<WeixinTemplate> list = weixinTemplateService.selectWeixinTemplateList(weixinTemplate);
        ExcelUtil<WeixinTemplate> util = new ExcelUtil<WeixinTemplate>(WeixinTemplate.class);
        return util.exportExcel(list, "weixinTemplate");
    }

    /**
     * 获取微信模板详细信息
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinTemplate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(weixinTemplateService.selectWeixinTemplateById(id));
    }

    /**
     * 新增微信模板
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinTemplate:add')")
    @Log(title = "微信模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WeixinTemplate weixinTemplate)
    {
        return toAjax(weixinTemplateService.insertWeixinTemplate(weixinTemplate));
    }

    /**
     * 修改微信模板
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinTemplate:edit')")
    @Log(title = "微信模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WeixinTemplate weixinTemplate)
    {
        return toAjax(weixinTemplateService.updateWeixinTemplate(weixinTemplate));
    }

    /**
     * 删除微信模板
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinTemplate:remove')")
    @Log(title = "微信模板", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(weixinTemplateService.deleteWeixinTemplateByIds(ids.toString()));
    }
}
