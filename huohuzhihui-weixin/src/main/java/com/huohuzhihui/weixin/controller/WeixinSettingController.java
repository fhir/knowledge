package com.huohuzhihui.weixin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.weixin.domain.WeixinSetting;
import com.huohuzhihui.weixin.service.IWeixinSettingService;

/**
 * 微信公众号设置Controller
 * 
 * @author huohuzhihui
 * @date 2021-01-01
 */
@RestController
@RequestMapping("/weixin/weixinSetting")
public class WeixinSettingController extends BaseController
{
    @Autowired
    private IWeixinSettingService weixinSettingService;

    /**
     * 查询微信公众号设置列表
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinSetting:list')")
    @GetMapping("/list")
    public TableDataInfo list(WeixinSetting weixinSetting)
    {
        startPage();
        List<WeixinSetting> list = weixinSettingService.selectWeixinSettingList(weixinSetting);
        return getDataTable(list);
    }

    /**
     * 导出微信公众号设置列表
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinSetting:export')")
    @Log(title = "微信公众号设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WeixinSetting weixinSetting)
    {
        List<WeixinSetting> list = weixinSettingService.selectWeixinSettingList(weixinSetting);
        ExcelUtil<WeixinSetting> util = new ExcelUtil<WeixinSetting>(WeixinSetting.class);
        return util.exportExcel(list, "weixinSetting");
    }

    /**
     * 获取微信公众号设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinSetting:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(weixinSettingService.selectWeixinSettingById(id));
    }

    /**
     * 新增微信公众号设置
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinSetting:add')")
    @Log(title = "微信公众号设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WeixinSetting weixinSetting)
    {
        return toAjax(weixinSettingService.insertWeixinSetting(weixinSetting));
    }

    /**
     * 修改微信公众号设置
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinSetting:edit')")
    @Log(title = "微信公众号设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WeixinSetting weixinSetting)
    {
        return toAjax(weixinSettingService.updateWeixinSetting(weixinSetting));
    }

    /**
     * 删除微信公众号设置
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinSetting:remove')")
    @Log(title = "微信公众号设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(weixinSettingService.deleteWeixinSettingByIds(ids.toString()));
    }
}
