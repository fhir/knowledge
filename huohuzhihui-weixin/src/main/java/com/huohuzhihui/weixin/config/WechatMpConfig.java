package com.huohuzhihui.weixin.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.huohuzhihui.common.filter.RepeatableFilter;
import com.huohuzhihui.weixin.domain.WeixinSetting;
import com.huohuzhihui.weixin.filter.WeixinAppIdFilter;
import com.huohuzhihui.weixin.service.IWeixinSettingService;

import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;


/**
 * 微信公众号开发配置
 * @author pandaa
 *
 */
@Configuration
public class WechatMpConfig {

	@Autowired
	private IWeixinSettingService weixinSettingService;
	
	@Primary
	@Bean
    public WxMpService wxSingerMpService() {
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxMpConfigStorage());
        return wxMpService;
    }
    
    @Bean
    public WxMpConfigStorage wxMpConfigStorage() {
        String appId = weixinSettingService.getValueByKey(WeixinSetting.KEY_WEIXIN_APPID).getWeixinValue();
        //logger.info("【微信支付配置】->【微信appID】："+appId);
        String appSecret = weixinSettingService.getValueByKey(WeixinSetting.KEY_WEIXIN_APPSECRET).getWeixinValue();
        //logger.info("【微信支付配置】->【微信秘钥appSecret】："+appSecret);
        WxMpDefaultConfigImpl wxMpConfigStorage = new WxMpDefaultConfigImpl();
        wxMpConfigStorage.setAppId(appId);
        wxMpConfigStorage.setSecret(appSecret);
        return wxMpConfigStorage;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Bean
    public FilterRegistrationBean appIdFilterRegistration()
    {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        
        WeixinAppIdFilter filter = new WeixinAppIdFilter();
        filter.wxMpConfigStorage = wxMpConfigStorage();
        registration.setFilter(filter);
        
        registration.addUrlPatterns("/*");
        registration.setName("weixinAppIdFilter");
        registration.setOrder(FilterRegistrationBean.HIGHEST_PRECEDENCE);
        return registration;
    }
}
