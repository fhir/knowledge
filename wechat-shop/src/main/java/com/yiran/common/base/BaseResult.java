package com.yiran.common.base;

import lombok.Data;

@Data
public class BaseResult {
	private boolean success = true;
	private String resultMessage = "";
	
	public BaseResult() {		
	}
	
	public BaseResult(boolean success) {
		this.success = success;
	}
	public BaseResult(boolean success, String msg) {
		this.success = success;
		this.resultMessage = msg;
	}
}
