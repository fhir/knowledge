package com.yiran.wechat.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.common.core.domain.EventEntity;

import java.util.Date;

/**
 * 商品促销活动表 wechat_product_promotion_event
 * 
 * @author yiran
 * @date 2019-06-13
 */
public class WechatProductPromotionEvent extends EventEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 自动编号 */
	private Integer productPromotionEventId;
	/** 活动名称：橱窗名称, 最新,热门,推荐,清仓,换季等 */
	private String name;
	/** 活动代码:英文唯一 */
	private String code;
	/** 封面图片url */
	private String imageUrl;
	/** 商品数量统计 */
	private String productCount;
	/** 排列次序 */
	private String sort;
	/** 状态:0不可显示，1显示 */
	private String status;
	

	public void setProductPromotionEventId(Integer productPromotionEventId) 
	{
		this.productPromotionEventId = productPromotionEventId;
	}

	public Integer getProductPromotionEventId() 
	{
		return productPromotionEventId;
	}
	public void setName(String name) 
	{
		this.name = name;
	}

	public String getName() 
	{
		return name;
	}
	public void setCode(String code) 
	{
		this.code = code;
	}

	public String getCode() 
	{
		return code;
	}
	public void setImageUrl(String imageUrl) 
	{
		this.imageUrl = imageUrl;
	}

	public String getImageUrl() 
	{
		return imageUrl;
	}
	public void setProductCount(String productCount) 
	{
		this.productCount = productCount;
	}

	public String getProductCount() 
	{
		return productCount;
	}
	
	public void setSort(String sort) 
	{
		this.sort = sort;
	}

	public String getSort() 
	{
		return sort;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}
	
	public void setStartTime(Date beginTime) 
	{
		this.setBeginTime(beginTime);
	}

	public Date getStartTime() 
	{
		return this.getBeginTime();
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("productPromotionEventId", getProductPromotionEventId())
            .append("name", getName())
            .append("code", getCode())
            .append("imageUrl", getImageUrl())
            .append("productCount", getProductCount())
            .append("remark", getRemark())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("sort", getSort())
            .append("status", getStatus())            
            .toString();
    }
}
