package com.yiran.framework.shiro.service;

import java.io.Serializable;
import java.util.Date;

import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.huohuzhihui.common.utils.StringUtils;
import com.huohuzhihui.system.domain.SysUserOnline;
import com.huohuzhihui.system.service.ISysUserOnlineService;
import com.yiran.framework.shiro.session.OnlineSession;


/**
 * 会话db操作处理
 * 
 * @author yiran
 */
@Component
public class SysShiroService
{
    @Autowired
    private ISysUserOnlineService onlineService;

    /**
     * 删除会话
     *
     * @param onlineSession 会话信息
     */
    public void deleteSession(OnlineSession onlineSession)
    {
    	//todo@byron
        //- onlineService.deleteById(String.valueOf(onlineSession.getId()));
    }

    /**
     * 获取会话信息
     *
     * @param sessionId
     * @return
     */
    public Session getSession(Serializable sessionId)
    {
    	//todo@byron
        SysUserOnline userOnline = null;//- onlineService.selectById(String.valueOf(sessionId));
        return StringUtils.isNull(userOnline) ? null : createSession(userOnline);
    }

    public Session createSession(SysUserOnline userOnline)
    {
        OnlineSession onlineSession = new OnlineSession();
        if (StringUtils.isNotNull(userOnline))
        {
            onlineSession.setId(userOnline.getSessionId());
            onlineSession.setHost(userOnline.getIpaddr());
            onlineSession.setBrowser(userOnline.getBrowser());
            onlineSession.setOs(userOnline.getOs());
            onlineSession.setDeptName(userOnline.getDeptName());
            onlineSession.setLoginName(userOnline.getUserName());
            onlineSession.setStartTimestamp(new Date(userOnline.getLoginTime()));
            onlineSession.setLastAccessTime(new Date(userOnline.getLastAccessTime()));
            onlineSession.setTimeout(userOnline.getExpireTime());
        }
        return onlineSession;
    }
}
