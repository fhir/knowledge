package com.yiran.payorder.domain;

import java.io.Serializable;
import java.util.List;

public class OrderQueryResult implements Serializable {

	private static final long serialVersionUID = -5202145773674974112L;

	private List<OrderVO> list;
	
	private long totalItems;

	public List<OrderVO> getList() {
		return list;
	}

	public void setList(List<OrderVO> list) {
		this.list = list;
	}

	public long getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(long totalItems) {
		this.totalItems = totalItems;
	}
	
	
}
