package com.yiran.payorder.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.huohuzhihui.common.annotation.DataSource;
import com.huohuzhihui.common.enums.DataSourceType;
import com.yiran.payorder.domaindo.VInstOrderResult;

@Mapper
@DataSource(value = DataSourceType.SHOP)
public interface VInstOrderResultMapper {

	public List<VInstOrderResult> selectInstOrderResultList(VInstOrderResult vInstOrderResult);
}
