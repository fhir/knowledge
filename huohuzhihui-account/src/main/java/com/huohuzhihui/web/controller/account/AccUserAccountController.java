package com.huohuzhihui.web.controller.account;

import java.math.BigDecimal;
import java.util.List;

import com.huohuzhihui.common.core.domain.entity.SysDictData;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.system.service.ISysDictDataService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.*;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.account.domain.AccUserAccount;
import com.huohuzhihui.account.service.IAccUserAccountService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.framework.message.EntityEventMessageListener;
import com.huohuzhihui.framework.message.IEntityEventSubscriber;
import com.huohuzhihui.framework.message.IUserEventSubscriber;
import com.huohuzhihui.framework.message.event.UserRegisterEvent;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 账户Controller
 * 
 * @author zylu
 * @date 2020-11-14
 */
@RestController
@RequestMapping("/account/account")
public class AccUserAccountController extends BaseController implements IUserEventSubscriber
{
    @Autowired
    private IAccUserAccountService accUserAccountService;
    @Autowired
    private ISysDictDataService iSysDictDataService;

    @Autowired
    EntityEventMessageListener listener; // 接受redis消息
    
    @Override
    @EventListener
    public boolean onUserCreated(UserRegisterEvent event) {
      insertAccount(event.getUser());
  	  return true;
    }

    
    /**
     * 插入账户信息
     * @param user
     */
    private void insertAccount(SysUser user){
        AccUserAccount account = new AccUserAccount();
        //account.set(userId);
        account.setUserId(user.getUserId());
        account.setBalance(BigDecimal.ZERO);

        account.setCreateTime(DateUtils.getNowDate());
        account.setCreateBy(user.getCreateBy());
        account.setDelFlag(0);
        account.setStatus(0);
        accUserAccountService.insertAccUserAccount(account);
    }

    
    /**
     * 查询账户列表
     */
    @PreAuthorize("@ss.hasPermi('account:account:list')")
    @GetMapping("/list")
    public TableDataInfo list(AccUserAccount accUserAccount)
    {
        startPage();
        List<AccUserAccount> list = accUserAccountService.selectAccUserAccountList(accUserAccount);
        return getDataTable(list);
    }

    /**
     * 导出账户列表
     */
    @PreAuthorize("@ss.hasPermi('account:account:export')")
    @Log(title = "账户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AccUserAccount accUserAccount)
    {
        List<AccUserAccount> list = accUserAccountService.selectAccUserAccountList(accUserAccount);
        ExcelUtil<AccUserAccount> util = new ExcelUtil<AccUserAccount>(AccUserAccount.class);
        return util.exportExcel(list, "account");
    }

    /**
     * 获取账户详细信息
     */
    @PreAuthorize("@ss.hasPermi('account:account:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(accUserAccountService.selectAccUserAccountById(id));
    }

    /**
     * 新增账户
     */
    @PreAuthorize("@ss.hasPermi('account:account:add')")
    @Log(title = "账户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AccUserAccount accUserAccount)
    {
        accUserAccount.setCreateBy(SecurityUtils.getUsername());
        return toAjax(accUserAccountService.insertAccUserAccount(accUserAccount));
    }

    /**
     * 修改账户
     */
    @PreAuthorize("@ss.hasPermi('account:account:edit')")
    @Log(title = "账户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AccUserAccount accUserAccount)
    {
        accUserAccount.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(accUserAccountService.updateAccUserAccount(accUserAccount));
    }

    /**
     * 删除账户
     */
    @PreAuthorize("@ss.hasPermi('account:account:remove')")
    @Log(title = "账户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(accUserAccountService.deleteAccUserAccountByIds(ids));
    }


    /**
     * 冻结账户
     */
    @PreAuthorize("@ss.hasPermi('account:account:remove')")
    @Log(title = "账户", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/freeze")
    @ResponseBody
    public AjaxResult freeze( Long[] ids)
    {
        return toAjax(accUserAccountService.freezeUserAccountByIds(ids));
    }

    /**
     * 冻结账户
     */
    @PreAuthorize("@ss.hasPermi('account:account:remove')")
    @Log(title = "账户", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/unfreeze")
    @ResponseBody
    public AjaxResult unfreeze(Long[] ids)
    {
        return toAjax(accUserAccountService.unfreezeUserAccountByIds(ids));
    }

    /**
     * 注销账户
     */
    @PreAuthorize("@ss.hasPermi('account:account:remove')")
    @Log(title = "账户", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/cancel")
    @ResponseBody
    public AjaxResult cancel(Long[] ids)
    {
        return toAjax(accUserAccountService.cancelUserAccountByIds(ids));
    }


    /**
     * 充值
     */
    @PreAuthorize("@ss.hasPermi('account:account:recharge')")
    @Log(title = "账户充值", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/recharge")
    public AjaxResult recharge(Long id, BigDecimal amount,String source,String channelCode)
    {
        return toAjax(accUserAccountService.recharge(   id,   amount,  source, channelCode, iSysDictDataService.selectDictLabel("pay_channel",channelCode),SecurityUtils.getUsername()));
    }
}
