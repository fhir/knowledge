package com.huohuzhihui.account.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.account.mapper.AccCollectMapper;
import com.huohuzhihui.account.domain.AccCollect;
import com.huohuzhihui.account.service.IAccCollectService;

/**
 * 收藏Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-20
 */
@Service
public class AccCollectServiceImpl implements IAccCollectService 
{
    @Autowired
    private AccCollectMapper accCollectMapper;

    /**
     * 查询收藏
     * 
     * @param id 收藏ID
     * @return 收藏
     */
    @Override
    public AccCollect selectAccCollectById(Long id)
    {
        return accCollectMapper.selectAccCollectById(id);
    }

    /**
     * 查询收藏列表
     * 
     * @param accCollect 收藏
     * @return 收藏
     */
    @Override
    public List<AccCollect> selectAccCollectList(AccCollect accCollect)
    {
        return accCollectMapper.selectAccCollectList(accCollect);
    }

    /**
     * 新增收藏
     * 
     * @param accCollect 收藏
     * @return 结果
     */
    @Override
    public int insertAccCollect(AccCollect accCollect)
    {
        accCollect.setCreateTime(DateUtils.getNowDate());
        return accCollectMapper.insertAccCollect(accCollect);
    }

    /**
     * 修改收藏
     * 
     * @param accCollect 收藏
     * @return 结果
     */
    @Override
    public int updateAccCollect(AccCollect accCollect)
    {
        return accCollectMapper.updateAccCollect(accCollect);
    }

    /**
     * 批量删除收藏
     * 
     * @param ids 需要删除的收藏ID
     * @return 结果
     */
    @Override
    public int deleteAccCollectByIds(Long[] ids)
    {
        return accCollectMapper.deleteAccCollectByIds(ids);
    }

    /**
     * 删除收藏信息
     * 
     * @param id 收藏ID
     * @return 结果
     */
    @Override
    public int deleteAccCollectById(Long id)
    {
        return accCollectMapper.deleteAccCollectById(id);
    }
}
