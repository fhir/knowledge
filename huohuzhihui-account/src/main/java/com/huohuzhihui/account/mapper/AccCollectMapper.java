package com.huohuzhihui.account.mapper;

import java.util.List;
import com.huohuzhihui.account.domain.AccCollect;

/**
 * 收藏Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-20
 */
public interface AccCollectMapper 
{
    /**
     * 查询收藏
     * 
     * @param id 收藏ID
     * @return 收藏
     */
    public AccCollect selectAccCollectById(Long id);

    /**
     * 查询收藏列表
     * 
     * @param accCollect 收藏
     * @return 收藏集合
     */
    public List<AccCollect> selectAccCollectList(AccCollect accCollect);

    /**
     * 新增收藏
     * 
     * @param accCollect 收藏
     * @return 结果
     */
    public int insertAccCollect(AccCollect accCollect);

    /**
     * 修改收藏
     * 
     * @param accCollect 收藏
     * @return 结果
     */
    public int updateAccCollect(AccCollect accCollect);

    /**
     * 删除收藏
     * 
     * @param id 收藏ID
     * @return 结果
     */
    public int deleteAccCollectById(Long id);

    /**
     * 批量删除收藏
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAccCollectByIds(Long[] ids);
}
